package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueLabelDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Labels;

public interface LabelsProcessor extends Processor<Labels> {
	
		public void processLabels(List<IssueLabelDefinition> labels);
	
}
