package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.CodeSystem;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Terminology;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TerminologyAssociation;

public interface TerminologyProcessor extends Processor<Terminology> {
	
		public void processCodeSystems(List<CodeSystem> codeSystems);
		public void processTerminologyAssociations(List<TerminologyAssociation> terminologyAssociations);
		public void processValueSets(List<net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet> list);
	
}
