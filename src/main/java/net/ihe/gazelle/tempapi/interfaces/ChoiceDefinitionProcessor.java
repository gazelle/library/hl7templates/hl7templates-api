package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;

public interface ChoiceDefinitionProcessor extends Processor<ChoiceDefinition> {
	
		public void processConstraints(List<FreeFormMarkupWithLanguage> constraints);
		public void processDescs(List<FreeFormMarkupWithLanguage> descs);
		public void processElements(List<RuleDefinition> elements);
		public void processIncludes(List<IncludeDefinition> includes);
		public void processItem(Item item);
		public void processMaximumMultiplicity(String maximumMultiplicity);
		public void processMinimumMultiplicity(Integer integer);
		public void processContains(List<ContainDefinition> contains);
}
