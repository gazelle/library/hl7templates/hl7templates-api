package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ConformanceType;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ScenarioTemplateCondition;

public interface ScenarioTemplateConditionProcessor extends Processor<ScenarioTemplateCondition> {
	
		public void processConformance(ConformanceType conformance);
		public void processIsMandatory(Boolean b);
		public void processMaximumMultiplicity(String maximumMultiplicity);
		public void processMinimumMultiplicity(Integer integer);
	
}
