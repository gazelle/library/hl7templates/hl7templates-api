package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateId;

public interface TemplateIdProcessor extends Processor<TemplateId> {
	
		public void processExtension(String extension);
		public void processRoot(String root);
	
}
