package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Author;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueAssignment;

public interface IssueAssignmentProcessor extends Processor<IssueAssignment> {
	
		public void processAuthor(Author author);
		public void processDescs(List<FreeFormMarkupWithLanguage> descs);
		public void processEffectiveDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processLabels(List<String> list);
		public void processName(String name);
		public void processTo(String to);
	
}
