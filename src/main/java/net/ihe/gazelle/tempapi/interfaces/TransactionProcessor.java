package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ActorsReference;
import net.ihe.gazelle.tempmodel.org.decor.art.model.BusinessNameWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ItemStatusCodeLifeCycle;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RepresentingTemplate;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Transaction;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TransactionTrigger;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TransactionType;

public interface TransactionProcessor extends Processor<Transaction> {
	
		public void processActors(ActorsReference actors);
		public void processConditions(List<FreeFormMarkupWithLanguage> conditions);
		public void processDependenciess(List<FreeFormMarkupWithLanguage> dependenciess);
		public void processDescs(List<FreeFormMarkupWithLanguage> descs);
		public void processEffectiveDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processExpirationDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processId(String id);
		public void processLabel(String label);
		public void processModel(String model);
		public void processNames(List<BusinessNameWithLanguage> list);
		public void processOfficialReleaseDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processRepresentingTemplate(RepresentingTemplate representingTemplate);
		public void processStatusCode(ItemStatusCodeLifeCycle statusCode);
		public void processTransactions(List<Transaction> transactions);
		public void processTrigger(TransactionTrigger trigger);
		public void processType(TransactionType type);
		public void processVersionLabel(String versionLabel);
	
}
