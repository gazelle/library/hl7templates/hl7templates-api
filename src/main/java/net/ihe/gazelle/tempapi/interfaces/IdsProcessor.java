package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.BaseId;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DefaultBaseId;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Id;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IdentifierAssociation;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Ids;

public interface IdsProcessor extends Processor<Ids> {
	
		public void processBaseIds(List<BaseId> baseIds);
		public void processDefaultBaseIds(List<DefaultBaseId> defaultBaseIds);
		public void processIds(List<Id> ids);
		public void processIdentifierAssociations(List<IdentifierAssociation> identifierAssociations);
	
}
