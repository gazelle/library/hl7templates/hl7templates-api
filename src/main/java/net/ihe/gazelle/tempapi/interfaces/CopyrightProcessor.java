package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.AddrLine;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Copyright;

public interface CopyrightProcessor extends Processor<Copyright> {
	
		public void processAddrLines(List<AddrLine> addrLines);
		public void processBy(String by);
		public void processLogo(String logo);
		public void processYears(List<String> list);
	
}
