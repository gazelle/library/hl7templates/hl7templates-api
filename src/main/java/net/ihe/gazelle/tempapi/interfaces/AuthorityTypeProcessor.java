package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.AddrLine;
import net.ihe.gazelle.tempmodel.org.decor.art.model.AuthorityType;

public interface AuthorityTypeProcessor extends Processor<AuthorityType> {
	
		public void processAddrLines(List<AddrLine> addrLines);
		public void processId(String id);
		public void processName(String name);
	
}
