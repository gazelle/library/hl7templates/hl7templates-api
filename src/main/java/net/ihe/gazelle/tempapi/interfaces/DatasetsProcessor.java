package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Dataset;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Datasets;

public interface DatasetsProcessor extends Processor<Datasets> {
	
		public void processDatasets(List<Dataset> datasets);
	
}
