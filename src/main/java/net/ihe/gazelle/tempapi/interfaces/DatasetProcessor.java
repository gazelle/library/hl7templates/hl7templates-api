package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ArbitraryPropertyType;
import net.ihe.gazelle.tempmodel.org.decor.art.model.BusinessNameWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Dataset;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ItemStatusCodeLifeCycle;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ObjectRelationships;

public interface DatasetProcessor extends Processor<Dataset> {
	
		public void processConcepts(List<DataSetConcept> concepts);
		public void processDescs(List<FreeFormMarkupWithLanguage> descs);
		public void processEffectiveDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processExpirationDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processId(String id);
		public void processNames(List<BusinessNameWithLanguage> list);
		public void processOfficialReleaseDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processPropertys(List<ArbitraryPropertyType> propertys);
		public void processRelationships(List<ObjectRelationships> relationships);
		public void processShortName(String shortName);
		public void processStatusCode(ItemStatusCodeLifeCycle statusCode);
		public void processVersionLabel(String versionLabel);
	
}
