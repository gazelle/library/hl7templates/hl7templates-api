package net.ihe.gazelle.tempapi.interfaces;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TerminologyAssociation;

public interface TerminologyAssociationProcessor extends Processor<TerminologyAssociation> {
	
		public void processCode(String code);
		public void processCodeSystem(String codeSystem);
		public void processCodeSystemName(String codeSystemName);
		public void processConceptFlexibility(XMLGregorianCalendar xmlGregorianCalendar);
		public void processConceptId(String conceptId);
		public void processDisplayName(String displayName);
		public void processEffectiveDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processExpirationDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processFlexibility(String flexibility);
		public void processOfficialReleaseDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processValueSet(String valueSet);
		public void processVersionLabel(String versionLabel);
	
}
