package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RepresentingTemplate;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ScenarioTemplateConcept;

public interface RepresentingTemplateProcessor extends Processor<RepresentingTemplate> {
	
		public void processConcepts(List<ScenarioTemplateConcept> concepts);
		public void processDisplayName(String displayName);
		public void processFlexibility(String flexibility);
		public void processRef(String ref);
		public void processSourceDataset(String sourceDataset);
		public void processSourceDatasetFlexibility(XMLGregorianCalendar xmlGregorianCalendar);
	
}
