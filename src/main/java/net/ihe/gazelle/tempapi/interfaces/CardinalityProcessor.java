package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Cardinality;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ConformanceType;

public interface CardinalityProcessor extends Processor<Cardinality> {
	
		public void processConformance(ConformanceType conformance);
		public void processIsMandatory(Boolean b);
		public void processMaximumMultiplicity(String maximumMultiplicity);
		public void processMinimumMultiplicity(Integer i);
	
}
