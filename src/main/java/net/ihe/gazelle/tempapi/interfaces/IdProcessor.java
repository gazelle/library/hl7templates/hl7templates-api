package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Designation;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Id;

public interface IdProcessor extends Processor<Id> {
	
		public void processAssigningAuthority(String assigningAuthority);
		public void processDesignations(List<Designation> designations);
		public void processExtension(String extension);
		public void processRoot(String root);
	
}
