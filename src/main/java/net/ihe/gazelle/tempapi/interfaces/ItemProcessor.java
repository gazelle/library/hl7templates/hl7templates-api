package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;

public interface ItemProcessor extends Processor<Item> {
	
		public void processDescs(List<FreeFormMarkupWithLanguage> descs);
		public void processLabel(String label);
	
}
