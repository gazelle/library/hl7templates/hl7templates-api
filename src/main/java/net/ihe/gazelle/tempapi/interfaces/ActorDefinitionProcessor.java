package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ActorDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.BusinessNameWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ScenarioActorType;

public interface ActorDefinitionProcessor extends Processor<ActorDefinition> {
	
		public void processDescs(List<FreeFormMarkupWithLanguage> descs);
		public void processId(String id);
		public void processNames(List<BusinessNameWithLanguage> list);
		public void processType(ScenarioActorType type);
	
}
