package net.ihe.gazelle.tempapi.interfaces;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateAssociationConcept;

public interface TemplateAssociationConceptProcessor extends Processor<TemplateAssociationConcept> {
	
		public void processEffectiveDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processElementId(String elementId);
		public void processRef(String ref);
	
}
