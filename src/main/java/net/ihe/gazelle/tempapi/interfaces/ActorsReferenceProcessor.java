package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ActorReference;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ActorsReference;

public interface ActorsReferenceProcessor extends Processor<ActorsReference> {
	
		public void processActors(List<ActorReference> actors);
	
}
