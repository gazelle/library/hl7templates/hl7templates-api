package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.CodeSystem;
import net.ihe.gazelle.tempmodel.org.decor.art.model.CodeSystemConceptList;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ItemStatusCodeLifeCycle;

public interface CodeSystemProcessor extends Processor<CodeSystem> {
	
		public void processConceptList(CodeSystemConceptList conceptList);
		public void processDescs(List<FreeFormMarkupWithLanguage> descs);
		public void processDisplayName(String displayName);
		public void processEffectiveDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processExpirationDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processFlexibility(String flexibility);
		public void processId(String id);
		public void processName(String name);
		public void processOfficialReleaseDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processRef(String ref);
		public void processStatusCode(ItemStatusCodeLifeCycle statusCode);
		public void processVersionLabel(String versionLabel);
	
}
