package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.BusinessNameWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ItemStatusCodeLifeCycle;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Scenario;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Transaction;

public interface ScenarioProcessor extends Processor<Scenario> {
	
		public void processConditions(List<FreeFormMarkupWithLanguage> conditions);
		public void processDescs(List<FreeFormMarkupWithLanguage> descs);
		public void processEffectiveDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processExpirationDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processId(String id);
		public void processNames(List<BusinessNameWithLanguage> list);
		public void processOfficialReleaseDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processStatusCode(ItemStatusCodeLifeCycle statusCode);
		public void processTransactions(List<Transaction> transactions);
		public void processTriggers(List<FreeFormMarkupWithLanguage> triggers);
		public void processVersionLabel(String versionLabel);
	
}
