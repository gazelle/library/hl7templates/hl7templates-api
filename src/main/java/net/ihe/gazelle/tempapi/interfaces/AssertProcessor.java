package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.AssertRole;

public interface AssertProcessor extends Processor<Assert> {
	
		public void processFlag(String flag);
		public void processRole(AssertRole role);
		public void processSee(String see);
		public void processTest(String test);
	
}
