package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Property;

public interface PropertyProcessor extends Processor<Property> {

	public final static String IDENTIFIER = "PropertyProcessor";

	public void processCurrency(String currency);
	public void processFractionDigits(String fractionDigits);
	public void processMaxInclude(String maxInclude);
	public void processMaxLength(Integer integer);
	public void processMinInclude(String minInclude);
	public void processMinLength(Integer integer);
	public void processUnit(String unit);
	public void processValue(String value);

}
