package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.*;

import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import java.util.List;

public interface RuleDefinitionProcessor extends Processor<RuleDefinition> {

	public final static String IDENTIFIER = "RuleDefinitionProcessor";

	public void process_asserts(List<Assert> _asserts);
	public void processAttributes(List<Attribute> attributes);
	public void processChoices(List<ChoiceDefinition> choices);
	public void processConformance(ConformanceType conformance);
	public void processConstraints(List<FreeFormMarkupWithLanguage> constraints);
	public void processContains(String contains);
	public void processDatatype(QName qName);
	public void processDefineVariables(List<DefineVariable> defineVariables);
	public void processDescs(List<FreeFormMarkupWithLanguage> descs);
	public void processDisplayName(String displayName);
	public void processEffectiveDate(XMLGregorianCalendar xmlGregorianCalendar);
	public void processElements(List<RuleDefinition> elements);
	public void processExamples(List<Example> examples);
	public void processExpirationDate(XMLGregorianCalendar xmlGregorianCalendar);
	public void processFlexibility(String flexibility);
	public void processId(String id);
	public void processIncludes(List<IncludeDefinition> includes);
	public void processIsClosed(Boolean b);
	public void processIsMandatory(Boolean b);
	public void processItem(Item item);
	public void processLets(List<Let> lets);
	public void processMaximumMultiplicity(String maximumMultiplicity);
	public void processMinimumMultiplicity(Integer integer);
	public void processName(String name);
	public void processOfficialReleaseDate(XMLGregorianCalendar xmlGregorianCalendar);
	public void processPropertys(List<Property> propertys);
	public void processReports(List<Report> reports);
	public void processStatusCode(ItemStatusCodeLifeCycle statusCode);
	public void processStrength(CodingStrengthType strength);
	public void processTexts(List<String> texts);
	public void processUseWhere(Boolean b);
	public void processVersionLabel(String versionLabel);
	public void processVocabularys(List<Vocabulary> vocabularys);
	public void processContainDefElements(List<ContainDefinition> contains);

}
