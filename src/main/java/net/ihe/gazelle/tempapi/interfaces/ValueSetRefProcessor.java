package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetRef;

public interface ValueSetRefProcessor extends Processor<ValueSetRef> {
	
		public void processDescs(List<FreeFormMarkupWithLanguage> descs);
		public void processException(Boolean b);
		public void processFlexibility(String flexibility);
		public void processRef(String ref);
	
}
