package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DefaultElementNamespace;

public interface DefaultElementNamespaceProcessor extends Processor<DefaultElementNamespace> {
	
		public void processNs(String ns);
	
}
