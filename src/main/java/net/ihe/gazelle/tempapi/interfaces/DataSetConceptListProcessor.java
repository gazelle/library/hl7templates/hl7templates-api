package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptList;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptListConcept;

public interface DataSetConceptListProcessor extends Processor<DataSetConceptList> {
	
		public void processConcepts(List<DataSetConceptListConcept> concepts);
		public void processId(String id);
		public void processRef(String ref);
	
}
