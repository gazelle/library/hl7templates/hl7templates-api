package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;

import java.util.List;

public interface ContainDefinitionProcessor extends Processor<ContainDefinition> {

    public void processIsMandatory(Boolean b);

    public void processItem(Item item);

    public void processMaximumMultiplicity(String maximumMultiplicity);

    public void processMinimumMultiplicity(Integer integer);

    public void processRef(String ref);

    public void processElements(List<RuleDefinition> elements);

}
