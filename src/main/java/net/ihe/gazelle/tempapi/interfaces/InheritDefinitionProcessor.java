package net.ihe.gazelle.tempapi.interfaces;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.InheritDefinition;

public interface InheritDefinitionProcessor extends Processor<InheritDefinition> {
	
		public void processEffectiveDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processRef(String ref);
	
}
