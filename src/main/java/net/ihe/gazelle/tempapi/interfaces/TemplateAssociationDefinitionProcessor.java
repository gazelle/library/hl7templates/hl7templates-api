package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateAssociationConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateAssociationDefinition;

public interface TemplateAssociationDefinitionProcessor extends Processor<TemplateAssociationDefinition> {
	
		public void processConcepts(List<TemplateAssociationConcept> concepts);
		public void processEffectiveDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processTemplateId(String templateId);
	
}
