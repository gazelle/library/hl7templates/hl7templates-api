package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ActorsDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Instances;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Scenario;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Scenarios;

public interface ScenariosProcessor extends Processor<Scenarios> {
	
		public void processActors(ActorsDefinition actors);
		public void processInstancess(List<Instances> instancess);
		public void processScenarios(List<Scenario> scenarios);
	
}
