package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.BusinessNameWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptListConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;

public interface DataSetConceptListConceptProcessor extends Processor<DataSetConceptListConcept> {
	
		public void processDescs(List<FreeFormMarkupWithLanguage> descs);
		public void processException(Boolean boolean1);
		public void processId(String id);
		public void processNames(List<BusinessNameWithLanguage> list);
		public void processSynonyms(List<BusinessNameWithLanguage> list);
	
}
