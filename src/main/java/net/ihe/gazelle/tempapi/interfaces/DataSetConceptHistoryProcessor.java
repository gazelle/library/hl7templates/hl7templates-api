package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptHistory;

public interface DataSetConceptHistoryProcessor extends Processor<DataSetConceptHistory> {
	
		public void processConcepts(List<DataSetConcept> concepts);
		public void processValidTimeHigh(XMLGregorianCalendar xmlGregorianCalendar);
	
}
