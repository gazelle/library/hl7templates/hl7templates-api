package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Author;
import net.ihe.gazelle.tempmodel.org.decor.art.model.NotifierOnOff;

public interface AuthorProcessor extends Processor<Author> {
	
		public void processEmail(String email);
		public void processId(String id);
		public void processNotifier(NotifierOnOff notifier);
		public void processUsername(String username);
	
}
