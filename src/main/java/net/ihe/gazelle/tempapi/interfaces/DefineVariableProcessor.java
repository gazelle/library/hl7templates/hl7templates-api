package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DefineVariable;
import net.ihe.gazelle.tempmodel.org.decor.art.model.VarCode;
import net.ihe.gazelle.tempmodel.org.decor.art.model.VarUse;

public interface DefineVariableProcessor extends Processor<DefineVariable> {
	
		public void processCode(VarCode code);
		public void processName(String name);
		public void processPath(String path);
		public void processUse(VarUse use);
	
}
