package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.VariousMixedContent;

public interface VariousMixedContentProcessor extends Processor<VariousMixedContent> {
	
	
}
