package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Let;

public interface LetProcessor extends Processor<Let> {
	
		public void processName(String name);
		public void processValue(String value);
	
}
