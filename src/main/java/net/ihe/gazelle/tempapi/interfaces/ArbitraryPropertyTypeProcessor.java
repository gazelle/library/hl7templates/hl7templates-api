package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ArbitraryPropertyType;

public interface ArbitraryPropertyTypeProcessor extends Processor<ArbitraryPropertyType> {
	
		public void processName(String name);
	
}
