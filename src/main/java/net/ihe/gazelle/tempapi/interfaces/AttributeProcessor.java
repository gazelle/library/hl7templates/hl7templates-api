package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import javax.xml.namespace.QName;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Vocabulary;

public interface AttributeProcessor extends Processor<Attribute> {


	public static String IDENTIFIER = "AttributeProcessor";

	public void processClassCode(String classCode);
	public void processConstraints(List<FreeFormMarkupWithLanguage> constraints);
	public void processContextConductionInd(Boolean contextConductionInd);
	public void processContextControlCode(String contextControlCode);
	public void processDatatype(QName qName);
	public void processDescs(List<FreeFormMarkupWithLanguage> descs);
	public void processDeterminerCode(String determinerCode);
	public void processExtension(String extension);
	public void processId(String id);
	public void processIndependentInd(Boolean independentInd);
	public void processInstitutionSpecified(Boolean boolean1);
	public void processInversionInd(Boolean boolean1);
	public void processIsOptional(Boolean boolean1);
	public void processItem(Item item);
	public void processMediaType(String mediaType);
	public void processMoodCode(String moodCode);
	public void processName(String name);
	public void processNegationInd(String negationInd);
	public void processNullFlavor(String nullFlavor);
	public void processOperator(String operator);
	public void processProhibited(Boolean boolean1);
	public void processQualifier(String qualifier);
	public void processRepresentation(String representation);
	public void processRoot(String root);
	public void processTypeCode(String typeCode);
	public void processUnit(String unit);
	public void processUse(String use);
	public void processValue(String value);
	public void processVocabularys(List<Vocabulary> vocabularys);

}
