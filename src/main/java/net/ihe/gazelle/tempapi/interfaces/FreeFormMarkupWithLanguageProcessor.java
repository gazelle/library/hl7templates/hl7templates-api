package net.ihe.gazelle.tempapi.interfaces;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;

public interface FreeFormMarkupWithLanguageProcessor extends Processor<FreeFormMarkupWithLanguage> {
	
		public void processLanguage(String language);
		public void processLastTranslated(XMLGregorianCalendar xmlGregorianCalendar);
		public void processMimeType(String mimeType);
	
}
