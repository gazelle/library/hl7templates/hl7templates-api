package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueLabelDefinition;

public interface IssueLabelDefinitionProcessor extends Processor<IssueLabelDefinition> {
	
		public void processCode(String code);
		public void processColor(String color);
		public void processDescs(List<FreeFormMarkupWithLanguage> descs);
		public void processName(String name);
	
}
