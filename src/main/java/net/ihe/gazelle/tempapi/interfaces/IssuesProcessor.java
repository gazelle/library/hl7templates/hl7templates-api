package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Issue;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Issues;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Labels;
import net.ihe.gazelle.tempmodel.org.decor.art.model.NotifierOnOff;

public interface IssuesProcessor extends Processor<Issues> {
	
		public void processIssues(List<Issue> issues);
		public void processLabels(Labels labels);
		public void processNotifier(NotifierOnOff notifier);
	
}
