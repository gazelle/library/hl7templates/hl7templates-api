package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ProjectHistory;

public interface ProjectHistoryProcessor extends Processor<ProjectHistory> {
	
		public void processBy(String by);
		public void processDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processDescs(List<FreeFormMarkupWithLanguage> descs);
	
}
