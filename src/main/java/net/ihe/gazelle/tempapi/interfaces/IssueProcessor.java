package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Issue;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueAssignment;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueObject;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssuePriority;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueTracking;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueType;

public interface IssueProcessor extends Processor<Issue> {
	
		public void processAssignments(List<IssueAssignment> assignments);
		public void processDisplayName(String displayName);
		public void processId(String id);
		public void processObjects(List<IssueObject> objects);
		public void processPriority(IssuePriority priority);
		public void processTrackings(List<IssueTracking> trackings);
		public void processType(IssueType type);
	
}
