package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetValueProperty;

public interface DataSetValuePropertyProcessor extends Processor<DataSetValueProperty> {
	
		public void process_default(String _default);
		public void processCurrency(String currency);
		public void processFixed(String fixed);
		public void processFractionDigits(String fractionDigits);
		public void processMaxInclude(String maxInclude);
		public void processMaxLength(Integer integer);
		public void processMinInclude(String minInclude);
		public void processMinLength(Integer integer);
		public void processTimeStampPrecision(String timeStampPrecision);
		public void processUnit(String unit);
	
}
