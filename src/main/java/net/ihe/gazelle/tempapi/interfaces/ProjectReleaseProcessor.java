package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ProjectRelease;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ReleaseStatusCodeLifeCycle;

public interface ProjectReleaseProcessor extends Processor<ProjectRelease> {
	
		public void processBy(String by);
		public void processDate(XMLGregorianCalendar xmlGregorianCalendar);
		public void processNotes(List<FreeFormMarkupWithLanguage> notes);
		public void processStatusCode(ReleaseStatusCodeLifeCycle statusCode);
		public void processVersionLabel(String versionLabel);
	
}
