package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Pathname;

public interface PathnameProcessor extends Processor<Pathname> {
	
		public void processPath(String path);
	
}
