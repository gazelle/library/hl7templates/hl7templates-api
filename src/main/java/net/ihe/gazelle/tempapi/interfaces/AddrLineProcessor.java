package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.AddrLine;
import net.ihe.gazelle.tempmodel.org.decor.art.model.AddressLineType;

public interface AddrLineProcessor extends Processor<AddrLine> {
	
		public void processType(AddressLineType type);
	
}
