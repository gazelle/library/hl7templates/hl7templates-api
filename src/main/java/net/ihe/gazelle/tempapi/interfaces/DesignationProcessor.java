package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Designation;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DesignationType;

public interface DesignationProcessor extends Processor<Designation> {
	
		public void processDisplayName(String displayName);
		public void processType(DesignationType type);
	
}
