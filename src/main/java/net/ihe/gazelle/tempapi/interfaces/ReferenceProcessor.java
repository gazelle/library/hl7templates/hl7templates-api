package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Reference;

public interface ReferenceProcessor extends Processor<Reference> {
	
		public void processLogo(String logo);
		public void processUrl(String url);
	
}
