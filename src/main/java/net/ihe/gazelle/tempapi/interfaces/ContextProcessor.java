package net.ihe.gazelle.tempapi.interfaces;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Context;

public interface ContextProcessor extends Processor<Context> {
	
		public void processId(String id);
		public void processPath(String path);
	
}
