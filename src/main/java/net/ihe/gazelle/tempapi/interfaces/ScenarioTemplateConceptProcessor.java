package net.ihe.gazelle.tempapi.interfaces;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.utils.Processor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ConformanceType;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ScenarioTemplateConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ScenarioTemplateCondition;

public interface ScenarioTemplateConceptProcessor extends Processor<ScenarioTemplateConcept> {
	
		public void processConditions(List<ScenarioTemplateCondition> conditions);
		public void processConformance(ConformanceType conformance);
		public void processFlexibility(XMLGregorianCalendar xmlGregorianCalendar);
		public void processIsMandatory(boolean b);
		public void processMaximumMultiplicity(String maximumMultiplicity);
		public void processMinimumMultiplicity(Integer integer);
		public void processRef(String ref);
	
}
