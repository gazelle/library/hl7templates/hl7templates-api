package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.DefineVariableProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DefineVariable;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DefineVariableTypeUtil;

public abstract class DefineVariableProcessorImpl implements DefineVariableProcessor {

	@Override
	public void process(DefineVariable t, Object... objects) {
		if (t != null){
			this.processCode(DefineVariableTypeUtil.getCodes(t));
			this.processName(t.getName());
			this.processPath(t.getPath());
			this.processUse(DefineVariableTypeUtil.getUses(t));
		}
	}

}
