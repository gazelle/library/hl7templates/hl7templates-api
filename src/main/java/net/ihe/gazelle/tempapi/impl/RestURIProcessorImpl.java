package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.RestURIProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RestURI;

public abstract class RestURIProcessorImpl implements RestURIProcessor {

	@Override
	public void process(RestURI t, Object... objects) {
		if (t != null){
			this.process_for(t.getFor());
			this.processFormat(t.getFormat());
		}
	}

}
