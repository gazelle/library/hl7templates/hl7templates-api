package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.IssueProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Issue;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.IssueType1Util;

public abstract class IssueProcessorImpl implements IssueProcessor {

	@Override
	public void process(Issue t, Object... objects) {
		if (t != null){
			this.processAssignments(IssueType1Util.getAssignments(t));
			this.processDisplayName(t.getDisplayName());
			this.processId(t.getId());
			this.processObjects(t.getObject());
			this.processPriority(t.getPriority());
			this.processTrackings(IssueType1Util.getTrackings(t));
			this.processType(t.getType());
		}
	}

}
