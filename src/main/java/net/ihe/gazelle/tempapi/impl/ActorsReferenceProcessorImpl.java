package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.ActorsReferenceProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ActorsReference;

public abstract class ActorsReferenceProcessorImpl implements ActorsReferenceProcessor {

	@Override
	public void process(ActorsReference t, Object... objects) {
		if (t != null){
			this.processActors(t.getActor());
		}
	}

}
