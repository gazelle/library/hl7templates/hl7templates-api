package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.TemplateAssociationConceptProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateAssociationConcept;

public abstract class TemplateAssociationConceptProcessorImpl implements TemplateAssociationConceptProcessor {

	@Override
	public void process(TemplateAssociationConcept t, Object... objects) {
		if (t != null){
			this.processEffectiveDate(t.getEffectiveDate());
			this.processElementId(t.getElementId());
			this.processRef(t.getRef());
		}
	}

}
