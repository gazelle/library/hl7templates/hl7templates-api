package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.CodedConceptProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.CodedConcept;

public abstract class CodedConceptProcessorImpl implements CodedConceptProcessor {

	@Override
	public void process(CodedConcept t, Object... objects) {
		if (t != null){
			this.processCode(t.getCode());
			this.processDesignations(t.getDesignation());
			this.processLevel(t.getLevel());
			this.processStatusCode(t.getStatusCode());
			this.processType(t.getType());
		}
	}

}
