package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.ScenarioTemplateConditionProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ScenarioTemplateCondition;

public abstract class ScenarioTemplateConditionProcessorImpl implements ScenarioTemplateConditionProcessor {

	@Override
	public void process(ScenarioTemplateCondition t, Object... objects) {
		if (t != null){
			this.processConformance(t.getConformance());
			this.processIsMandatory(t.isIsMandatory());
			this.processMaximumMultiplicity(t.getMaximumMultiplicity());
			this.processMinimumMultiplicity(t.getMinimumMultiplicity());
		}
	}

}
