package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.ScenarioTemplateConceptProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ScenarioTemplateConcept;

public abstract class ScenarioTemplateConceptProcessorImpl implements ScenarioTemplateConceptProcessor {

	@Override
	public void process(ScenarioTemplateConcept t, Object... objects) {
		if (t != null){
			this.processConditions(t.getCondition());
			this.processConformance(t.getConformance());
			this.processFlexibility(t.getFlexibility());
			this.processIsMandatory(t.isIsMandatory());
			this.processMaximumMultiplicity(t.getMaximumMultiplicity());
			this.processMinimumMultiplicity(t.getMinimumMultiplicity());
			this.processRef(t.getRef());
		}
	}

}
