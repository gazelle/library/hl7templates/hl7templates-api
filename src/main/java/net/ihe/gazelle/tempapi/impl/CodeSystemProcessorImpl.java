package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.CodeSystemProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.CodeSystem;

public abstract class CodeSystemProcessorImpl implements CodeSystemProcessor {

	@Override
	public void process(CodeSystem t, Object... objects) {
		if (t != null){
			this.processConceptList(t.getConceptList());
			this.processDescs(t.getDesc());
			this.processDisplayName(t.getDisplayName());
			this.processEffectiveDate(t.getEffectiveDate());
			this.processExpirationDate(t.getExpirationDate());
			this.processFlexibility(t.getFlexibility());
			this.processId(t.getId());
			this.processName(t.getName());
			this.processOfficialReleaseDate(t.getOfficialReleaseDate());
			this.processRef(t.getRef());
			this.processStatusCode(t.getStatusCode());
			this.processVersionLabel(t.getVersionLabel());
		}
	}

}
