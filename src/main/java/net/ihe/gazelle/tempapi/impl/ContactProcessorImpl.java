package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.ContactProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Contact;

public abstract class ContactProcessorImpl implements ContactProcessor {

	@Override
	public void process(Contact t, Object... objects) {
		if (t != null){
			this.processEmail(t.getEmail());
		}
	}

}
