package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.IdProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Id;

public abstract class IdProcessorImpl implements IdProcessor {

	@Override
	public void process(Id t, Object... objects) {
		if (t != null){
			this.processAssigningAuthority(t.getAssigningAuthority());
			this.processDesignations(t.getDesignation());
			this.processExtension(t.getExtension());
			this.processRoot(t.getRoot());
		}
	}

}
