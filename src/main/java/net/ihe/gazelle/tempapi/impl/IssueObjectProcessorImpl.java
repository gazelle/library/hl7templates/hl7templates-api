package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.IssueObjectProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueObject;

public abstract class IssueObjectProcessorImpl implements IssueObjectProcessor {

	@Override
	public void process(IssueObject t, Object... objects) {
		if (t != null){
			this.processEffectiveDate(t.getEffectiveDate());
			this.processId(t.getId());
			this.processName(t.getName());
			this.processType(t.getType());
		}
	}

}
