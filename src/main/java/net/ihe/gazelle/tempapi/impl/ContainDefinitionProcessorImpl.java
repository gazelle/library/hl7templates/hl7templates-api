package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.ContainDefinitionProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ContainDefinitionUtil;

import java.util.List;

public abstract class ContainDefinitionProcessorImpl implements ContainDefinitionProcessor {

    @Override
    public void process(ContainDefinition t, Object... objects) {
        if (t != null) {
            this.processIsMandatory(t.getIsMandatory());
            this.processMaximumMultiplicity(t.getMaximumMultiplicity());
            this.processMinimumMultiplicity(t.getMinimumMultiplicity());
            this.processRef(t.getRef());
            this.processItem(t.getItem());
            this.processElements(ContainDefinitionUtil.getElements(t));
        }
    }

    @Override
    public void processIsMandatory(Boolean b) {
        // to be overrided if needed to
    }

    @Override
    public void processItem(Item item) {
        // to be overrided if needed to
    }

    @Override
    public void processMaximumMultiplicity(String maximumMultiplicity) {
        // to be overrided if needed to
    }

    @Override
    public void processMinimumMultiplicity(Integer integer) {
        // to be overrided if needed to
    }

    @Override
    public void processRef(String ref) {
        // to be overrided if needed to
    }

    @Override
    public void processElements(List<RuleDefinition> elements) {
        // to be overrided if needed to
    }

}
