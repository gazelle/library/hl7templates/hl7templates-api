package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.FreeFormMarkupWithLanguageProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;

public abstract class FreeFormMarkupWithLanguageProcessorImpl implements FreeFormMarkupWithLanguageProcessor {

	@Override
	public void process(FreeFormMarkupWithLanguage t, Object... objects) {
		if (t != null){
			this.processLanguage(t.getLanguage());
			this.processLastTranslated(t.getLastTranslated());
			this.processMimeType(t.getMimeType());
		}
	}

}
