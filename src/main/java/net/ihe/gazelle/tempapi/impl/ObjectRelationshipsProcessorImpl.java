package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.ObjectRelationshipsProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ObjectRelationships;

public abstract class ObjectRelationshipsProcessorImpl implements ObjectRelationshipsProcessor {

	@Override
	public void process(ObjectRelationships t, Object... objects) {
		if (t != null){
			this.processFlexibility(t.getFlexibility());
			this.processRef(t.getRef());
			this.processType(t.getType());
		}
	}

}
