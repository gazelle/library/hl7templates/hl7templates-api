package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.TemplateAssociationDefinitionProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateAssociationDefinition;

public abstract class TemplateAssociationDefinitionProcessorImpl implements TemplateAssociationDefinitionProcessor {

	@Override
	public void process(TemplateAssociationDefinition t, Object... objects) {
		if (t != null){
			this.processConcepts(t.getConcept());
			this.processEffectiveDate(t.getEffectiveDate());
			this.processTemplateId(t.getTemplateId());
		}
	}

}
