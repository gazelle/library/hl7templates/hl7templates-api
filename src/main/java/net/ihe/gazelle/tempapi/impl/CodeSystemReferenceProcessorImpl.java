package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.CodeSystemReferenceProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.CodeSystemReference;

public abstract class CodeSystemReferenceProcessorImpl implements CodeSystemReferenceProcessor {

	@Override
	public void process(CodeSystemReference t, Object... objects) {
		if (t != null){
			this.processCodeSystem(t.getCodeSystem());
			this.processCodeSystemName(t.getCodeSystemName());
			this.processCodeSystemVersion(t.getCodeSystemVersion());
			this.processFlexibility(t.getFlexibility());
		}
	}

}
