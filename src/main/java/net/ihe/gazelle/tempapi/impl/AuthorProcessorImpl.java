package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.AuthorProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Author;

public abstract class AuthorProcessorImpl implements AuthorProcessor {

	@Override
	public void process(Author t, Object... objects) {
		if (t != null){
			this.processEmail(t.getEmail());
			this.processId(t.getId());
			this.processNotifier(t.getNotifier());
			this.processUsername(t.getUsername());
		}
	}

}
