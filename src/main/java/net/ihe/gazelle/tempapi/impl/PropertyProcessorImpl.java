package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.PropertyProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Property;

public abstract class PropertyProcessorImpl implements PropertyProcessor {

	@Override
	public void process(Property t, Object... objects) {
		if (t != null){
			this.processCurrency(t.getCurrency());
			this.processFractionDigits(t.getFractionDigits());
			this.processMaxInclude(t.getMaxInclude());
			this.processMaxLength(t.getMaxLength());
			this.processMinInclude(t.getMinInclude());
			this.processMinLength(t.getMinLength());
			this.processUnit(t.getUnit());
			this.processValue(t.getValue());
		}
	}

	@Override
	public void processCurrency(String currency) {
		// to be overrided if needed to
	}

	@Override
	public void processFractionDigits(String fractionDigits) {
		// to be overrided if needed to
	}

	@Override
	public void processMaxInclude(String maxInclude) {
		// to be overrided if needed to
	}

	@Override
	public void processMaxLength(Integer integer) {
		// to be overrided if needed to
	}

	@Override
	public void processMinInclude(String minInclude) {
		// to be overrided if needed to
	}

	@Override
	public void processMinLength(Integer integer) {
		// to be overrided if needed to
	}

	@Override
	public void processUnit(String unit) {
		// to be overrided if needed to
	}

	@Override
	public void processValue(String value) {
		// to be overrided if needed to
	}

}
