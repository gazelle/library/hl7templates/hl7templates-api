package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.IssueLabelDefinitionProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueLabelDefinition;

public abstract class IssueLabelDefinitionProcessorImpl implements IssueLabelDefinitionProcessor {

	@Override
	public void process(IssueLabelDefinition t, Object... objects) {
		if (t != null){
			this.processCode(t.getCode());
			this.processColor(t.getColor());
			this.processDescs(t.getDesc());
			this.processName(t.getName());
		}
	}

}
