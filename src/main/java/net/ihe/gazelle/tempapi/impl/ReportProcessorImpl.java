package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.ReportProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.AssertRole;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Report;

public abstract class ReportProcessorImpl implements ReportProcessor {

	@Override
	public void process(Report t, Object... objects) {
		if (t != null){
			this.processFlag(t.getFlag());
			this.processRole(t.getRole());
			this.processSee(t.getSee());
			this.processTest(t.getTest());
		}
	}

	@Override
	public void processFlag(String flag) {
		// nothing to do
		
	}

	@Override
	public void processRole(AssertRole role) {
		// nothing to do
		
	}

	@Override
	public void processSee(String see) {
		// nothing to do
		
	}

	@Override
	public void processTest(String test) {
		// nothing to do
		
	}

}
