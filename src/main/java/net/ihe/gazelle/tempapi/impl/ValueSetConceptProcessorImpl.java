package net.ihe.gazelle.tempapi.impl;

import java.math.BigInteger;
import java.util.List;

import net.ihe.gazelle.tempapi.interfaces.ValueSetConceptProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.VocabType;

public abstract class ValueSetConceptProcessorImpl implements ValueSetConceptProcessor {

	@Override
	public void process(ValueSetConcept t, Object... objects) {
		if (t != null){
			this.processCode(t.getCode());
			this.processCodeSystem(t.getCodeSystem());
			this.processCodeSystemName(t.getCodeSystemName());
			this.processCodeSystemVersion(t.getCodeSystemVersion());
			this.processDescs(t.getDesc());
			this.processDisplayName(t.getDisplayName());
			this.processLevel(t.getLevel());
			this.processType(t.getType());
		}
	}

	@Override
	public void processCode(String code) {
		// to be overrided if needed to
	}

	@Override
	public void processCodeSystem(String codeSystem) {
		// to be overrided if needed to
	}

	@Override
	public void processCodeSystemName(String codeSystemName) {
		// to be overrided if needed to
	}

	@Override
	public void processCodeSystemVersion(String codeSystemVersion) {
		// to be overrided if needed to
	}

	@Override
	public void processDescs(List<FreeFormMarkupWithLanguage> descs) {
		// to be overrided if needed to
	}

	@Override
	public void processDisplayName(String displayName) {
		// to be overrided if needed to
	}

	@Override
	public void processLevel(BigInteger bigInteger) {
		// to be overrided if needed to
	}

	@Override
	public void processType(VocabType type) {
		// to be overrided if needed to
	}

}
