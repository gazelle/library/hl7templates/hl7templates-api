package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.ProjectReleaseProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ProjectRelease;

public abstract class ProjectReleaseProcessorImpl implements ProjectReleaseProcessor {

	@Override
	public void process(ProjectRelease t, Object... objects) {
		if (t != null){
			this.processBy(t.getBy());
			this.processDate(t.getDate());
			this.processNotes(t.getNote());
			this.processStatusCode(t.getStatusCode());
			this.processVersionLabel(t.getVersionLabel());
		}
	}

}
