package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.AssertProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.AssertRole;

public abstract class AssertProcessorImpl implements AssertProcessor {

	@Override
	public void process(Assert t, Object... objects) {
		if (t != null){
			this.processFlag(t.getFlag());
			this.processRole(t.getRole());
			this.processSee(t.getSee());
			this.processTest(t.getTest());
		}
	}

	@Override
	public void processFlag(String flag) {
		// to be overrided if needed to
	}

	@Override
	public void processRole(AssertRole role) {
		// to be overrided if needed to
	}

	@Override
	public void processSee(String see) {
		// to be overrided if needed to
	}

	@Override
	public void processTest(String test) {
		// to be overrided if needed to
	}

}
