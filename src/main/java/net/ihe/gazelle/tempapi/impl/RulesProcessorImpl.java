package net.ihe.gazelle.tempapi.impl;

import java.util.List;

import net.ihe.gazelle.tempapi.interfaces.RulesProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Rules;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateAssociationDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;

public class RulesProcessorImpl implements RulesProcessor {

	@Override
	public void process(Rules t, Object... objects) {
		if (t != null){
			this.processTemplates(RulesUtil.getTemplates(t));
			this.processTemplateAssociations(RulesUtil.getTemplateAssociations(t));
		}
	}

	@Override
	public void processTemplates(List<TemplateDefinition> templates) {
		// to be overrided if needed to
	}

	@Override
	public void processTemplateAssociations(List<TemplateAssociationDefinition> templateAssociations) {
		// to be overrided if needed to
	}

}
