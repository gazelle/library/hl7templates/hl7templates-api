package net.ihe.gazelle.tempapi.impl;

import java.util.List;

import net.ihe.gazelle.tempapi.interfaces.ValueSetRefProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetRef;

public abstract class ValueSetRefProcessorImpl implements ValueSetRefProcessor {

	@Override
	public void process(ValueSetRef t, Object... objects) {
		if (t != null){
			this.processDescs(t.getDesc());
			this.processException(t.isException());
			this.processFlexibility(t.getFlexibility());
			this.processRef(t.getRef());
		}
	}

	@Override
	public void processDescs(List<FreeFormMarkupWithLanguage> descs) {
		// nothing to do
		
	}

	@Override
	public void processException(Boolean b) {
		// nothing to do
		
	}

	@Override
	public void processFlexibility(String flexibility) {
		// nothing to do
		
	}

	@Override
	public void processRef(String ref) {
		// nothing to do
		
	}

}
