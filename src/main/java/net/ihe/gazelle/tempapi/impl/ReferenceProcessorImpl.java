package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.ReferenceProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Reference;

public abstract class ReferenceProcessorImpl implements ReferenceProcessor {

	@Override
	public void process(Reference t, Object... objects) {
		if (t != null){
			this.processLogo(t.getLogo());
			this.processUrl(t.getUrl());
		}
	}

}
