package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.DatasetProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Dataset;

public abstract class DatasetProcessorImpl implements DatasetProcessor {

	@Override
	public void process(Dataset t, Object... objects) {
		if (t != null){
			this.processConcepts(t.getConcept());
			this.processDescs(t.getDesc());
			this.processEffectiveDate(t.getEffectiveDate());
			this.processExpirationDate(t.getExpirationDate());
			this.processId(t.getId());
			this.processNames(t.getName());
			this.processOfficialReleaseDate(t.getOfficialReleaseDate());
			this.processPropertys(t.getProperty());
			this.processRelationships(t.getRelationship());
			this.processShortName(t.getShortName());
			this.processStatusCode(t.getStatusCode());
			this.processVersionLabel(t.getVersionLabel());
		}
	}

}
