package net.ihe.gazelle.tempapi.impl;

import java.util.List;

import net.ihe.gazelle.tempapi.interfaces.ProjectProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Author;
import net.ihe.gazelle.tempmodel.org.decor.art.model.BuildingBlockRepository;
import net.ihe.gazelle.tempmodel.org.decor.art.model.BusinessNameWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Contact;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Copyright;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DefaultElementNamespace;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Project;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ProjectHistory;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ProjectRelease;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Reference;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RestURI;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ProjectTypeUtil;

public abstract class ProjectProcessorImpl implements ProjectProcessor {

	@Override
	public void process(Project t, Object... objects) {
		if (t != null){
			this.processAuthors(t.getAuthor());
			this.processBuildingBlockRepositorys(t.getBuildingBlockRepository());
			this.processContacts(t.getContact());
			this.processCopyrights(t.getCopyright());
			this.processDefaultElementNamespace(t.getDefaultElementNamespace());
			this.processDefaultLanguage(t.getDefaultLanguage());
			this.processDescs(t.getDesc());
			this.processExperimental(t.isExperimental());
			this.processId(t.getId());
			this.processNames(t.getName());
			this.processPrefix(t.getPrefix());
			this.processReference(t.getReference());
			this.processReleases(ProjectTypeUtil.getReleases(t));
			this.processRestURIs(t.getRestURI());
			this.processVersions(ProjectTypeUtil.getVersions(t));
		}
	}

	@Override
	public void processAuthors(List<Author> authors) {
		// to be overrided if needed to
		
	}

	@Override
	public void processBuildingBlockRepositorys(
			List<BuildingBlockRepository> buildingBlockRepositorys) {
		// to be overrided if needed to
		
	}

	@Override
	public void processContacts(List<Contact> contacts) {
		// to be overrided if needed to
		
	}

	@Override
	public void processCopyrights(List<Copyright> copyrights) {
		// to be overrided if needed to
		
	}

	@Override
	public void processDefaultElementNamespace(
			DefaultElementNamespace defaultElementNamespace) {
		// to be overrided if needed to
		
	}

	@Override
	public void processDefaultLanguage(String defaultLanguage) {
		// to be overrided if needed to
		
	}

	@Override
	public void processDescs(List<FreeFormMarkupWithLanguage> descs) {
		// to be overrided if needed to
		
	}

	@Override
	public void processExperimental(Boolean b) {
		// to be overrided if needed to
		
	}

	@Override
	public void processId(String id) {
		// to be overrided if needed to
		
	}

	@Override
	public void processNames(List<BusinessNameWithLanguage> list) {
		// to be overrided if needed to
		
	}

	@Override
	public void processPrefix(String prefix) {
		// to be overrided if needed to
		
	}

	@Override
	public void processReference(Reference reference) {
		// to be overrided if needed to
		
	}

	@Override
	public void processReleases(List<ProjectRelease> releases) {
		// to be overrided if needed to
		
	}

	@Override
	public void processRestURIs(List<RestURI> restURIs) {
		// to be overrided if needed to
		
	}

	@Override
	public void processVersions(List<ProjectHistory> versions) {
		// to be overrided if needed to
		
	}

}
