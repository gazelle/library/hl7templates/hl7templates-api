package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.CopyrightProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Copyright;

public abstract class CopyrightProcessorImpl implements CopyrightProcessor {

	@Override
	public void process(Copyright t, Object... objects) {
		if (t != null){
			this.processAddrLines(t.getAddrLine());
			this.processBy(t.getBy());
			this.processLogo(t.getLogo());
			this.processYears(t.getYears());
		}
	}

}
