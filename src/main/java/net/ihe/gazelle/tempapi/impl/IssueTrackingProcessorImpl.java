package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.IssueTrackingProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueTracking;

public abstract class IssueTrackingProcessorImpl implements IssueTrackingProcessor {

	@Override
	public void process(IssueTracking t, Object... objects) {
		if (t != null){
			this.processAuthor(t.getAuthor());
			this.processDescs(t.getDesc());
			this.processEffectiveDate(t.getEffectiveDate());
			this.processLabels(t.getLabels());
			this.processStatusCode(t.getStatusCode());
		}
	}

}
