package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.IdsProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Ids;

public abstract class IdsProcessorImpl implements IdsProcessor {

	@Override
	public void process(Ids t, Object... objects) {
		if (t != null){
			this.processBaseIds(t.getBaseId());
			this.processDefaultBaseIds(t.getDefaultBaseId());
			this.processIds(t.getId());
			this.processIdentifierAssociations(t.getIdentifierAssociation());
		}
	}

}
