package net.ihe.gazelle.tempapi.impl;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.interfaces.TemplateDefinitionProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Context;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DefineVariable;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Example;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Let;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Report;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateProperties;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateRelationships;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateStatusCodeLifeCycle;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

public abstract class TemplateDefinitionProcessorImpl implements TemplateDefinitionProcessor {

	@Override
	public void process(TemplateDefinition t, Object... objects) {
		if (t != null){
			this.process_asserts(TemplateDefinitionUtil.getAsserts(t));
			this.processAttributes(TemplateDefinitionUtil.getAttributes(t));
			this.processChoices(TemplateDefinitionUtil.getChoices(t));
			this.processClassifications(t.getClassification());
			this.processConstraints(TemplateDefinitionUtil.getConstraints(t));
			this.processContext(t.getContext());
			this.processDefineVariables(TemplateDefinitionUtil.getDefineVariables(t));
			this.processDescs(t.getDesc());
			this.processDisplayName(t.getDisplayName());
			this.processEffectiveDate(t.getEffectiveDate());
			this.processElements(TemplateDefinitionUtil.getElements(t));
			this.processExamples(t.getExample());
			this.processExpirationDate(t.getExpirationDate());
			this.processId(t.getId());
			this.processIdent(t.getIdent());
			this.processIncludes(TemplateDefinitionUtil.getIncludes(t));
			this.processIsClosed(t.isIsClosed());
			this.processLets(TemplateDefinitionUtil.getLets(t));
			this.processName(t.getName());
			this.processOfficialReleaseDate(t.getOfficialReleaseDate());
			this.processRef(t.getRef());
			this.processReferencedFrom(t.getReferencedFrom());
			this.processRelationships(t.getRelationship());
			this.processReports(TemplateDefinitionUtil.getReports(t));
			this.processStatusCode(t.getStatusCode());
			this.processUrl(t.getUrl());
			this.processVersionLabel(t.getVersionLabel());
			this.processContains(TemplateDefinitionUtil.getContains(t));
			this.processItem(t.getItem());
		}
	}
	
	@Override
	public void processContains(List<ContainDefinition> contains) {
		// to be overrided if needed to
	}

	@Override
	public void process_asserts(List<Assert> _asserts) {
		// to be overrided if needed to
	}

	@Override
	public void processAttributes(List<Attribute> attributes) {
		// to be overrided if needed to
	}

	@Override
	public void processChoices(List<ChoiceDefinition> choices) {
		// to be overrided if needed to
	}

	@Override
	public void processClassifications(List<TemplateProperties> classifications) {
		// to be overrided if needed to
	}

	@Override
	public void processConstraints(List<FreeFormMarkupWithLanguage> constraints) {
		// to be overrided if needed to
	}

	@Override
	public void processContext(Context context) {
		// to be overrided if needed to
	}

	@Override
	public void processDefineVariables(List<DefineVariable> defineVariables) {
		// to be overrided if needed to
	}

	@Override
	public void processDescs(List<FreeFormMarkupWithLanguage> descs) {
		// to be overrided if needed to
	}

	@Override
	public void processDisplayName(String displayName) {
		// to be overrided if needed to
	}

	@Override
	public void processEffectiveDate(XMLGregorianCalendar xmlGregorianCalendar) {
		// to be overrided if needed to
	}

	@Override
	public void processElements(List<RuleDefinition> elements) {
		// to be overrided if needed to
	}

	@Override
	public void processExamples(List<Example> examples) {
		// to be overrided if needed to
	}

	@Override
	public void processExpirationDate(XMLGregorianCalendar xmlGregorianCalendar) {
		// to be overrided if needed to
	}

	@Override
	public void processId(String id) {
		// to be overrided if needed to
	}

	@Override
	public void processIdent(String ident) {
		// to be overrided if needed to
	}

	@Override
	public void processIncludes(List<IncludeDefinition> includes) {
		// to be overrided if needed to
	}

	@Override
	public void processIsClosed(Boolean b) {
		// to be overrided if needed to
	}

	@Override
	public void processItem(Item item) {
		// to be overrided if needed to
	}

	@Override
	public void processLets(List<Let> lets) {
		// to be overrided if needed to
	}

	@Override
	public void processName(String name) {
		// to be overrided if needed to
	}

	@Override
	public void processOfficialReleaseDate(
			XMLGregorianCalendar xmlGregorianCalendar) {
		// to be overrided if needed to
	}

	@Override
	public void processRef(String ref) {
		// to be overrided if needed to
	}

	@Override
	public void processReferencedFrom(String referencedFrom) {
		// to be overrided if needed to
	}

	@Override
	public void processRelationships(List<TemplateRelationships> relationships) {
		// to be overrided if needed to
	}

	@Override
	public void processReports(List<Report> reports) {
		// to be overrided if needed to
	}

	@Override
	public void processStatusCode(TemplateStatusCodeLifeCycle statusCode) {
		// to be overrided if needed to
	}

	@Override
	public void processUrl(String url) {
		// to be overrided if needed to
	}

	@Override
	public void processVersionLabel(String versionLabel) {
		// to be overrided if needed to
	}

}
