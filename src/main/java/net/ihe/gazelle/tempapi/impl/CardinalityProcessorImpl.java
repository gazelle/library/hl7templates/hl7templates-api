package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.CardinalityProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Cardinality;

public abstract class CardinalityProcessorImpl implements CardinalityProcessor {

	@Override
	public void process(Cardinality t, Object... objects) {
		if (t != null){
			this.processConformance(t.getConformance());
			this.processIsMandatory(t.isIsMandatory());
			this.processMaximumMultiplicity(t.getMaximumMultiplicity());
			this.processMinimumMultiplicity(t.getMinimumMultiplicity());
		}
	}

}
