package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.ProjectHistoryProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ProjectHistory;

public abstract class ProjectHistoryProcessorImpl implements ProjectHistoryProcessor {

	@Override
	public void process(ProjectHistory t, Object... objects) {
		if (t != null){
			this.processBy(t.getBy());
			this.processDate(t.getDate());
			this.processDescs(t.getDesc());
		}
	}

}
