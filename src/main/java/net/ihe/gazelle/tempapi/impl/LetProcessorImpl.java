package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.LetProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Let;

public abstract class LetProcessorImpl implements LetProcessor {

	@Override
	public void process(Let t, Object... objects) {
		if (t != null){
			this.processName(t.getName());
			this.processValue(t.getValue());
		}
	}

	@Override
	public void processName(String name) {
		// to be overrided if needed to
	}

	@Override
	public void processValue(String value) {
		// to be overrided if needed to
	}

}
