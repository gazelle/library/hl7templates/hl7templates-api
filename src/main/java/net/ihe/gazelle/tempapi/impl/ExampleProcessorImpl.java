package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.ExampleProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Example;

public abstract class ExampleProcessorImpl implements ExampleProcessor {

	@Override
	public void process(Example t, Object... objects) {
		if (t != null){
			this.processCaption(t.getCaption());
			this.processType(t.getType());
		}
	}

}
