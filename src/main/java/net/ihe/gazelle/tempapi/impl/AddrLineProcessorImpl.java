package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.AddrLineProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.AddrLine;

public abstract class AddrLineProcessorImpl implements AddrLineProcessor {

	@Override
	public void process(AddrLine t, Object... objects) {
		if (t != null){
			this.processType(t.getType());
		}
	}

}
