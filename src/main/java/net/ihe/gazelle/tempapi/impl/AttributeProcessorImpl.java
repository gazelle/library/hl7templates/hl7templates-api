package net.ihe.gazelle.tempapi.impl;

import java.util.List;

import javax.xml.namespace.QName;

import net.ihe.gazelle.tempapi.interfaces.AttributeProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Vocabulary;

public abstract class AttributeProcessorImpl implements AttributeProcessor {

	@Override
	public void process(Attribute t, Object... objects) {
		if (t != null){
			this.processClassCode(t.getClassCode());
			this.processConstraints(t.getConstraint());
			this.processContextConductionInd(t.isContextConductionInd());
			this.processContextControlCode(t.getContextControlCode());
			this.processDatatype(t.getDatatype());
			this.processDescs(t.getDesc());
			this.processDeterminerCode(t.getDeterminerCode());
			this.processExtension(t.getExtension());
			this.processId(t.getId());
			this.processIndependentInd(t.isIndependentInd());
			this.processInstitutionSpecified(t.isInstitutionSpecified());
			this.processInversionInd(t.isInversionInd());
			this.processIsOptional(t.isIsOptional());
			this.processMediaType(t.getMediaType());
			this.processMoodCode(t.getMoodCode());
			this.processName(t.getName());
			this.processNegationInd(t.getNegationInd());
			this.processNullFlavor(t.getNullFlavor());
			this.processOperator(t.getOperator());
			this.processProhibited(t.isProhibited());
			this.processQualifier(t.getQualifier());
			this.processRepresentation(t.getRepresentation());
			this.processRoot(t.getRoot());
			this.processTypeCode(t.getTypeCode());
			this.processUnit(t.getUnit());
			this.processUse(t.getUse());
			this.processValue(t.getValue());
			this.processVocabularys(t.getVocabulary());
			this.processItem(t.getItem());
		}
	}

	@Override
	public void processClassCode(String classCode) {
		// to be overrided if needed to
	}

	@Override
	public void processConstraints(List<FreeFormMarkupWithLanguage> constraints) {
		// to be overrided if needed to
	}

	@Override
	public void processContextConductionInd(Boolean contextConductionInd) {
		// to be overrided if needed to
	}

	@Override
	public void processContextControlCode(String contextControlCode) {
		// to be overrided if needed to
	}

	@Override
	public void processDatatype(QName qName) {
		// to be overrided if needed to
	}

	@Override
	public void processDescs(List<FreeFormMarkupWithLanguage> descs) {
		// to be overrided if needed to
	}

	@Override
	public void processDeterminerCode(String determinerCode) {
		// to be overrided if needed to
	}

	@Override
	public void processExtension(String extension) {
		// to be overrided if needed to
	}

	@Override
	public void processId(String id) {
		// to be overrided if needed to
	}

	@Override
	public void processIndependentInd(Boolean independentInd) {
		// to be overrided if needed to
	}

	@Override
	public void processInstitutionSpecified(Boolean boolean1) {
		// to be overrided if needed to
	}

	@Override
	public void processInversionInd(Boolean boolean1) {
		// to be overrided if needed to
	}

	@Override
	public void processIsOptional(Boolean boolean1) {
		// to be overrided if needed to
	}

	@Override
	public void processItem(Item item) {
		// to be overrided if needed to
	}

	@Override
	public void processMediaType(String mediaType) {
		// to be overrided if needed to
	}

	@Override
	public void processMoodCode(String moodCode) {
		// to be overrided if needed to
	}

	@Override
	public void processName(String name) {
		// to be overrided if needed to
	}

	@Override
	public void processNegationInd(String negationInd) {
		// to be overrided if needed to
	}

	@Override
	public void processNullFlavor(String nullFlavor) {
		// to be overrided if needed to
	}

	@Override
	public void processOperator(String operator) {
		// to be overrided if needed to
	}

	@Override
	public void processProhibited(Boolean boolean1) {
		// to be overrided if needed to
	}

	@Override
	public void processQualifier(String qualifier) {
		// to be overrided if needed to
	}

	@Override
	public void processRepresentation(String representation) {
		// to be overrided if needed to
	}

	@Override
	public void processRoot(String root) {
		// to be overrided if needed to
	}

	@Override
	public void processTypeCode(String typeCode) {
		// to be overrided if needed to
	}

	@Override
	public void processUnit(String unit) {
		// to be overrided if needed to
	}

	@Override
	public void processUse(String use) {
		// to be overrided if needed to
	}

	@Override
	public void processValue(String value) {
		// to be overrided if needed to
	}

	@Override
	public void processVocabularys(List<Vocabulary> vocabularys) {
		// to be overrided if needed to
	}

}
