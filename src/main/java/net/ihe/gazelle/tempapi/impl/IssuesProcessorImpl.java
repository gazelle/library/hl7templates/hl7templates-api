package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.IssuesProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Issues;

public abstract class IssuesProcessorImpl implements IssuesProcessor {

	@Override
	public void process(Issues t, Object... objects) {
		if (t != null){
			this.processIssues(t.getIssue());
			this.processLabels(t.getLabels());
			this.processNotifier(t.getNotifier());
		}
	}

}
