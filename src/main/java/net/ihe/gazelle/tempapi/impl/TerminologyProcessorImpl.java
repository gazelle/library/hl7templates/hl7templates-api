package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.TerminologyProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Terminology;

public abstract class TerminologyProcessorImpl implements TerminologyProcessor {

	@Override
	public void process(Terminology t, Object... objects) {
		if (t != null){
			this.processCodeSystems(t.getCodeSystem());
			this.processTerminologyAssociations(t.getTerminologyAssociation());
			this.processValueSets(t.getValueSet());
		}
	}

}
