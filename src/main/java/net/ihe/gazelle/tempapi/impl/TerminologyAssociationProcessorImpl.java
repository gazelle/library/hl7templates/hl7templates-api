package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.TerminologyAssociationProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TerminologyAssociation;

public abstract class TerminologyAssociationProcessorImpl implements TerminologyAssociationProcessor {

	@Override
	public void process(TerminologyAssociation t, Object... objects) {
		if (t != null){
			this.processCode(t.getCode());
			this.processCodeSystem(t.getCodeSystem());
			this.processCodeSystemName(t.getCodeSystemName());
			this.processConceptFlexibility(t.getConceptFlexibility());
			this.processConceptId(t.getConceptId());
			this.processDisplayName(t.getDisplayName());
			this.processEffectiveDate(t.getEffectiveDate());
			this.processExpirationDate(t.getExpirationDate());
			this.processFlexibility(t.getFlexibility());
			this.processOfficialReleaseDate(t.getOfficialReleaseDate());
			this.processValueSet(t.getValueSet());
			this.processVersionLabel(t.getVersionLabel());
		}
	}

}
