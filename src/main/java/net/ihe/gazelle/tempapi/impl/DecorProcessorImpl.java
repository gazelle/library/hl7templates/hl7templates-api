package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.DecorProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Datasets;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Ids;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Issues;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Project;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Rules;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Scenarios;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Terminology;

public abstract class DecorProcessorImpl implements DecorProcessor {

	@Override
	public void process(Decor t, Object... objects) {
		if (t != null){
			this.processCompilationDate(t.getCompilationDate());
			this.processDatasets(t.getDatasets());
			this.processDeeplinkprefix(t.getDeeplinkprefix());
			this.processDeeplinkprefixservices(t.getDeeplinkprefixservices());
			this.processIds(t.getIds());
			this.processIssues(t.getIssues());
			this.processLanguage(t.getLanguage());
			this.processProject(t.getProject());
			this.processRelease(t.getRelease());
			this.processRepository(t.isRepository());
			this.processRules(t.getRules());
			this.processScenarios(t.getScenarios());
			this.processTerminology(t.getTerminology());
			this.processVersionDate(t.getVersionDate());
			this.processVersionLabel(t.getVersionLabel());
		}
	}

	@Override
	public void processCompilationDate(String compilationDate) {
		// to be overrided if needed to
	}

	@Override
	public void processDatasets(Datasets datasets) {
		// to be overrided if needed to
	}

	@Override
	public void processDeeplinkprefix(String deeplinkprefix) {
		// to be overrided if needed to
	}

	@Override
	public void processDeeplinkprefixservices(String deeplinkprefixservices) {
		// to be overrided if needed to
	}

	@Override
	public void processIds(Ids ids) {
		// to be overrided if needed to
	}

	@Override
	public void processIssues(Issues issues) {
		// to be overrided if needed to
	}

	@Override
	public void processLanguage(String language) {
		// to be overrided if needed to
	}

	@Override
	public void processProject(Project project) {
		// to be overrided if needed to
	}

	@Override
	public void processRelease(Integer i) {
		// to be overrided if needed to
	}

	@Override
	public void processRepository(Boolean boolean1) {
		// to be overrided if needed to
	}

	@Override
	public void processRules(Rules rules) {
		// to be overrided if needed to
	}

	@Override
	public void processScenarios(Scenarios scenarios) {
		// to be overrided if needed to
	}

	@Override
	public void processTerminology(Terminology terminology) {
		// to be overrided if needed to
	}

	@Override
	public void processVersionDate(String versionDate) {
		// to be overrided if needed to
	}

	@Override
	public void processVersionLabel(String versionLabel) {
		// to be overrided if needed to
	}

}
