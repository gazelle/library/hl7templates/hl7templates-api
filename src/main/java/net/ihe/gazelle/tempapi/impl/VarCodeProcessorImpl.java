package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.VarCodeProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.VarCode;

public abstract class VarCodeProcessorImpl implements VarCodeProcessor {

	@Override
	public void process(VarCode t, Object... objects) {
		if (t != null){
			this.processCode(t.getCode());
			this.processCodeSystem(t.getCodeSystem());
		}
	}

}
