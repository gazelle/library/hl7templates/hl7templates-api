package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.ActorReferenceProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ActorReference;

public abstract class ActorReferenceProcessorImpl implements ActorReferenceProcessor {

	@Override
	public void process(ActorReference t, Object... objects) {
		if (t != null){
			this.processId(t.getId());
			this.processRole(t.getRole());
		}
	}

}
