package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.ActorsDefinitionProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ActorsDefinition;

public abstract class ActorsDefinitionProcessorImpl implements ActorsDefinitionProcessor {

	@Override
	public void process(ActorsDefinition t, Object... objects) {
		if (t != null){
			this.processActors(t.getActor());
		}
	}

}
