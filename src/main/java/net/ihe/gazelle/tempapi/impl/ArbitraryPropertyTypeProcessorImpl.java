package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.ArbitraryPropertyTypeProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ArbitraryPropertyType;

public abstract class ArbitraryPropertyTypeProcessorImpl implements ArbitraryPropertyTypeProcessor {

	@Override
	public void process(ArbitraryPropertyType t, Object... objects) {
		if (t != null){
			this.processName(t.getName());
		}
	}

}
