package net.ihe.gazelle.tempapi.impl;

import java.util.List;

import net.ihe.gazelle.tempapi.interfaces.ChoiceDefinitionProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ChoiceDefinitionUtil;

public abstract class ChoiceDefinitionProcessorImpl implements ChoiceDefinitionProcessor {

	@Override
	public void process(ChoiceDefinition t, Object... objects) {
		if (t != null){
			this.processConstraints(ChoiceDefinitionUtil.getConstraints(t));
			this.processDescs(t.getDesc());
			this.processElements(ChoiceDefinitionUtil.getElements(t));
			this.processIncludes(ChoiceDefinitionUtil.getIncludes(t));
			this.processMaximumMultiplicity(t.getMaximumMultiplicity());
			this.processMinimumMultiplicity(t.getMinimumMultiplicity());
			this.processContains(ChoiceDefinitionUtil.getContains(t));
			this.processItem(t.getItem());
		}
	}

	@Override
	public void processConstraints(List<FreeFormMarkupWithLanguage> constraints) {
		// to be overrided if needed to
	}

	@Override
	public void processDescs(List<FreeFormMarkupWithLanguage> descs) {
		// to be overrided if needed to
	}

	@Override
	public void processElements(List<RuleDefinition> elements) {
		// to be overrided if needed to
	}

	@Override
	public void processIncludes(List<IncludeDefinition> includes) {
		// to be overrided if needed to
	}

	@Override
	public void processItem(Item item) {
		// to be overrided if needed to
	}

	@Override
	public void processMaximumMultiplicity(String maximumMultiplicity) {
		// to be overrided if needed to
	}

	@Override
	public void processMinimumMultiplicity(Integer integer) {
		// to be overrided if needed to
	}
	
	@Override
	public void processContains(List<ContainDefinition> contains) {
		// to be overrided if needed to
	}

}
