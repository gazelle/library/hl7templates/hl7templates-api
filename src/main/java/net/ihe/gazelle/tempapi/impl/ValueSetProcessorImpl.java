package net.ihe.gazelle.tempapi.impl;

import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import net.ihe.gazelle.tempapi.interfaces.ValueSetProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.CodeSystemReference;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ItemStatusCodeLifeCycle;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConceptList;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet.SourceCodeSystem;

public abstract class ValueSetProcessorImpl implements ValueSetProcessor {

	@Override
	public void process(ValueSet t, Object... objects) {
		if (t != null){
			this.processCompleteCodeSystems(t.getCompleteCodeSystem());
			this.processConceptList(t.getConceptList());
			this.processCopyright(t.getCopyright());
			this.processDescs(t.getDesc());
			this.processDisplayName(t.getDisplayName());
			this.processEffectiveDate(t.getEffectiveDate());
			this.processExpirationDate(t.getExpirationDate());
			this.processFlexibility(t.getFlexibility());
			this.processId(t.getId());
			this.processIdent(t.getIdent());
			this.processName(t.getName());
			this.processOfficialReleaseDate(t.getOfficialReleaseDate());
			this.processRef(t.getRef());
			this.processReferencedFrom(t.getReferencedFrom());
			this.processSourceCodeSystems(t.getSourceCodeSystem());
			this.processStatusCode(t.getStatusCode());
			this.processUrl(t.getUrl());
			this.processVersionLabel(t.getVersionLabel());
		}
	}

	@Override
	public void processCompleteCodeSystems(
			List<CodeSystemReference> completeCodeSystems) {
		// to be overrided if needed to
	}

	@Override
	public void processConceptList(ValueSetConceptList conceptList) {
		// to be overrided if needed to
	}

	@Override
	public void processCopyright(String copyright) {
		// to be overrided if needed to
	}

	@Override
	public void processDescs(List<FreeFormMarkupWithLanguage> descs) {
		// to be overrided if needed to
	}

	@Override
	public void processDisplayName(String displayName) {
		// to be overrided if needed to
	}

	@Override
	public void processEffectiveDate(XMLGregorianCalendar xmlGregorianCalendar) {
		// to be overrided if needed to
	}

	@Override
	public void processExpirationDate(XMLGregorianCalendar xmlGregorianCalendar) {
		// to be overrided if needed to
	}

	@Override
	public void processFlexibility(String flexibility) {
		// to be overrided if needed to
	}

	@Override
	public void processId(String id) {
		// to be overrided if needed to
	}

	@Override
	public void processIdent(String ident) {
		// to be overrided if needed to
	}

	@Override
	public void processName(String name) {
		// to be overrided if needed to
	}

	@Override
	public void processOfficialReleaseDate(
			XMLGregorianCalendar xmlGregorianCalendar) {
		// to be overrided if needed to
	}

	@Override
	public void processRef(String ref) {
		// to be overrided if needed to
	}

	@Override
	public void processReferencedFrom(String referencedFrom) {
		// to be overrided if needed to
	}

	@Override
	public void processSourceCodeSystems(
			List<SourceCodeSystem> sourceCodeSystems) {
		// to be overrided if needed to
	}

	@Override
	public void processStatusCode(ItemStatusCodeLifeCycle statusCode) {
		// to be overrided if needed to
	}

	@Override
	public void processUrl(String url) {
		// to be overrided if needed to
	}

	@Override
	public void processVersionLabel(String versionLabel) {
		// to be overrided if needed to
	}

}
