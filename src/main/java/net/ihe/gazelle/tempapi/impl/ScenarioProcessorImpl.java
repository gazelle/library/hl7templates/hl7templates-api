package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.ScenarioProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Scenario;

public abstract class ScenarioProcessorImpl implements ScenarioProcessor {

	@Override
	public void process(Scenario t, Object... objects) {
		if (t != null){
			this.processConditions(t.getCondition());
			this.processDescs(t.getDesc());
			this.processEffectiveDate(t.getEffectiveDate());
			this.processExpirationDate(t.getExpirationDate());
			this.processId(t.getId());
			this.processNames(t.getName());
			this.processOfficialReleaseDate(t.getOfficialReleaseDate());
			this.processStatusCode(t.getStatusCode());
			this.processTransactions(t.getTransaction());
			this.processTriggers(t.getTrigger());
			this.processVersionLabel(t.getVersionLabel());
		}
	}

}
