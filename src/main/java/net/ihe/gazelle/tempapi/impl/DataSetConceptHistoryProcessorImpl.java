package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.DataSetConceptHistoryProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptHistory;

public abstract class DataSetConceptHistoryProcessorImpl implements DataSetConceptHistoryProcessor {

	@Override
	public void process(DataSetConceptHistory t, Object... objects) {
		if (t != null){
			this.processConcepts(t.getConcept());
			this.processValidTimeHigh(t.getValidTimeHigh());
		}
	}

}
