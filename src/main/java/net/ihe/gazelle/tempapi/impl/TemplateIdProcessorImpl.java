package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.TemplateIdProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateId;

public abstract class TemplateIdProcessorImpl implements TemplateIdProcessor {

	@Override
	public void process(TemplateId t, Object... objects) {
		if (t != null){
			this.processExtension(t.getExtension());
			this.processRoot(t.getRoot());
		}
	}

}
