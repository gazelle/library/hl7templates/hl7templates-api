package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.DefaultElementNamespaceProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DefaultElementNamespace;

public abstract class DefaultElementNamespaceProcessorImpl implements DefaultElementNamespaceProcessor {

	@Override
	public void process(DefaultElementNamespace t, Object... objects) {
		if (t != null){
			this.processNs(t.getNs());
		}
	}

}
