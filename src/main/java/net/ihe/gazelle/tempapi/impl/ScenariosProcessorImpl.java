package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.ScenariosProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Scenarios;

public abstract class ScenariosProcessorImpl implements ScenariosProcessor {

	@Override
	public void process(Scenarios t, Object... objects) {
		if (t != null){
			this.processActors(t.getActors());
			this.processInstancess(t.getInstances());
			this.processScenarios(t.getScenario());
		}
	}

}
