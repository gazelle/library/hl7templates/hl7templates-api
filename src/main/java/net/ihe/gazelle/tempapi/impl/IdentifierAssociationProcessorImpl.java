package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.IdentifierAssociationProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IdentifierAssociation;

public abstract class IdentifierAssociationProcessorImpl implements IdentifierAssociationProcessor {

	@Override
	public void process(IdentifierAssociation t, Object... objects) {
		if (t != null){
			this.processConceptId(t.getConceptId());
			this.processEffectiveDate(t.getEffectiveDate());
			this.processExpirationDate(t.getExpirationDate());
			this.processOfficialReleaseDate(t.getOfficialReleaseDate());
			this.processRef(t.getRef());
			this.processVersionLabel(t.getVersionLabel());
		}
	}

}
