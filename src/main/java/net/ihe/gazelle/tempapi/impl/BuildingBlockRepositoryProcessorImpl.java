package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.BuildingBlockRepositoryProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.BuildingBlockRepository;

public abstract class BuildingBlockRepositoryProcessorImpl implements BuildingBlockRepositoryProcessor {

	@Override
	public void process(BuildingBlockRepository t, Object... objects) {
		if (t != null){
			this.processFormat(t.getFormat());
			this.processIdent(t.getIdent());
			this.processLicenseKey(t.getLicenseKey());
			this.processUrl(t.getUrl());
		}
	}

}
