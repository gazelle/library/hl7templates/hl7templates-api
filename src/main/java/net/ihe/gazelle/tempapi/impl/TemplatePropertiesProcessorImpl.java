package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.TemplatePropertiesProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateProperties;

public abstract class TemplatePropertiesProcessorImpl implements TemplatePropertiesProcessor {

	@Override
	public void process(TemplateProperties t, Object... objects) {
		if (t != null){
			this.processFormat(t.getFormat());
			this.processPropertys(t.getProperty());
			this.processTags(t.getTag());
			this.processType(t.getType());
		}
	}

}
