package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.IssueAssignmentProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueAssignment;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.IssueAssignmentUtil;

public abstract class IssueAssignmentProcessorImpl implements IssueAssignmentProcessor {

	@Override
	public void process(IssueAssignment t, Object... objects) {
		if (t != null){
			this.processAuthor(IssueAssignmentUtil.getAuthors(t));
			this.processDescs(IssueAssignmentUtil.getDescs(t));
			this.processEffectiveDate(t.getEffectiveDate());
			this.processLabels(t.getLabels());
			this.processName(t.getName());
			this.processTo(t.getTo());
		}
	}

}
