package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.ObjectHistoryProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ObjectHistory;

public abstract class ObjectHistoryProcessorImpl implements ObjectHistoryProcessor {

	@Override
	public void process(ObjectHistory t, Object... objects) {
		if (t != null){
			this.processBy(t.getBy());
			this.processDate(t.getDate());
			this.processDescs(t.getDesc());
		}
	}

}
