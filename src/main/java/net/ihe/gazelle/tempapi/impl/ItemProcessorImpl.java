package net.ihe.gazelle.tempapi.impl;

import net.ihe.gazelle.tempapi.interfaces.ItemProcessor;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;

public abstract class ItemProcessorImpl implements ItemProcessor {

	@Override
	public void process(Item t, Object... objects) {
		if (t != null){
			this.processDescs(t.getDesc());
			this.processLabel(t.getLabel());
		}
	}

}
