package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ValueSetRefProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetRef;

public class ValueSetRefVisitor implements HL7TemplateVisitor<ValueSetRef> {

	public static final ValueSetRefVisitor INSTANCE = new ValueSetRefVisitor();

	@Override
	public void visitAndProcess(ValueSetRef t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ValueSetRefProcessor processor = implProvider.provideImpl(ValueSetRefProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process desc
		if (t.getDesc() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getDesc()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
	}

}
