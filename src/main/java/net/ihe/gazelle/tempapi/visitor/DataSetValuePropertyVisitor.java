package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.DataSetValuePropertyProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetValueProperty;

public class DataSetValuePropertyVisitor implements HL7TemplateVisitor<DataSetValueProperty> {

	public static final DataSetValuePropertyVisitor INSTANCE = new DataSetValuePropertyVisitor();

	@Override
	public void visitAndProcess(DataSetValueProperty t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		DataSetValuePropertyProcessor processor = implProvider.provideImpl(DataSetValuePropertyProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
