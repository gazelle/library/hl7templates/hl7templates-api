package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ContactProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Contact;

public class ContactVisitor implements HL7TemplateVisitor<Contact> {

	public static final ContactVisitor INSTANCE = new ContactVisitor();

	@Override
	public void visitAndProcess(Contact t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ContactProcessor processor = implProvider.provideImpl(ContactProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
