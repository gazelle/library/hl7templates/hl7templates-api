package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.TemplateDefinitionProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DefineVariable;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Example;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Let;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Report;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateProperties;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateRelationships;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.TemplateDefinitionUtil;

public class TemplateDefinitionVisitor implements HL7TemplateVisitor<TemplateDefinition> {

	public static final TemplateDefinitionVisitor INSTANCE = new TemplateDefinitionVisitor();

	@Override
	public void visitAndProcess(TemplateDefinition t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		TemplateDefinitionProcessor processor = implProvider.provideImpl(TemplateDefinitionProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process _assert
		if (TemplateDefinitionUtil.getAsserts(t) != null) {
			for(Assert _assert : TemplateDefinitionUtil.getAsserts(t)) {
				AssertVisitor.INSTANCE.visitAndProcess(_assert, implProvider, objects);
			}
		}
		// process attribute
		if (TemplateDefinitionUtil.getAttributes(t) != null) {
			for(Attribute _attribute : TemplateDefinitionUtil.getAttributes(t)) {
				AttributeVisitor.INSTANCE.visitAndProcess(_attribute, implProvider, objects);
			}
		}
		// process choice
		if (TemplateDefinitionUtil.getChoices(t) != null) {
			for(ChoiceDefinition _choiceDefinition : TemplateDefinitionUtil.getChoices(t)) {
				ChoiceDefinitionVisitor.INSTANCE.visitAndProcess(_choiceDefinition, implProvider, objects);
			}
		}
		// process classification
		if (t.getClassification() != null) {
			for(TemplateProperties _templateProperties : t.getClassification()) {
				TemplatePropertiesVisitor.INSTANCE.visitAndProcess(_templateProperties, implProvider, objects);
			}
		}
		// process constraint
		if (TemplateDefinitionUtil.getConstraints(t) != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : TemplateDefinitionUtil.getConstraints(t)) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process context
		if (t.getContext() != null) {
			ContextVisitor.INSTANCE.visitAndProcess(t.getContext(), implProvider, objects);
		}
		// process defineVariable
		if (TemplateDefinitionUtil.getDefineVariables(t) != null) {
			for(DefineVariable _defineVariable : TemplateDefinitionUtil.getDefineVariables(t)) {
				DefineVariableVisitor.INSTANCE.visitAndProcess(_defineVariable, implProvider, objects);
			}
		}
		// process desc
		if (t.getDesc() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getDesc()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process element
		if (TemplateDefinitionUtil.getElements(t) != null) {
			for(RuleDefinition _ruleDefinition : TemplateDefinitionUtil.getElements(t)) {
				RuleDefinitionVisitor.INSTANCE.visitAndProcess(_ruleDefinition, implProvider, objects);
			}
		}
		// process example
		if (t.getExample() != null) {
			for(Example _example : t.getExample()) {
				ExampleVisitor.INSTANCE.visitAndProcess(_example, implProvider, objects);
			}
		}
		// process include
		if (TemplateDefinitionUtil.getIncludes(t) != null) {
			for(IncludeDefinition _includeDefinition : TemplateDefinitionUtil.getIncludes(t)) {
				IncludeDefinitionVisitor.INSTANCE.visitAndProcess(_includeDefinition, implProvider, objects);
			}
		}
		
		// process contain
		if (TemplateDefinitionUtil.getContains(t) != null) {
			for(ContainDefinition _containDefinition : TemplateDefinitionUtil.getContains(t)) {
				ContainDefinitionVisitor.INSTANCE.visitAndProcess(_containDefinition, implProvider, objects);
			}
		}
		
		// process item
		if (t.getItem() != null) {
			ItemVisitor.INSTANCE.visitAndProcess(t.getItem(), implProvider, objects);
		}
		// process let
		if (TemplateDefinitionUtil.getLets(t) != null) {
			for(Let _let : TemplateDefinitionUtil.getLets(t)) {
				LetVisitor.INSTANCE.visitAndProcess(_let, implProvider, objects);
			}
		}
		// process relationship
		if (t.getRelationship() != null) {
			for(TemplateRelationships _templateRelationships : t.getRelationship()) {
				TemplateRelationshipsVisitor.INSTANCE.visitAndProcess(_templateRelationships, implProvider, objects);
			}
		}
		// process report
		if (TemplateDefinitionUtil.getReports(t) != null) {
			for(Report _report : TemplateDefinitionUtil.getReports(t)) {
				ReportVisitor.INSTANCE.visitAndProcess(_report, implProvider, objects);
			}
		}
	}

}
