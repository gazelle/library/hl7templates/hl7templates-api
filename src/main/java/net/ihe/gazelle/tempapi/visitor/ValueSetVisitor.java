package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ValueSetProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.CodeSystemReference;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet.SourceCodeSystem;

public class ValueSetVisitor implements HL7TemplateVisitor<ValueSet> {

	public static final ValueSetVisitor INSTANCE = new ValueSetVisitor();

	@Override
	public void visitAndProcess(ValueSet t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ValueSetProcessor processor = implProvider.provideImpl(ValueSetProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process completeCodeSystem
		if (t.getCompleteCodeSystem() != null) {
			for(CodeSystemReference _codeSystemReference : t.getCompleteCodeSystem()) {
				CodeSystemReferenceVisitor.INSTANCE.visitAndProcess(_codeSystemReference, implProvider, objects);
			}
		}
		// process conceptList
		if (t.getConceptList() != null) {
			ValueSetConceptListVisitor.INSTANCE.visitAndProcess(t.getConceptList(), implProvider, objects);
		}
		// process desc
		if (t.getDesc() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getDesc()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process sourceCodeSystem
		if (t.getSourceCodeSystem() != null) {
			for(SourceCodeSystem _sourceCodeSystem : t.getSourceCodeSystem()) {
				SourceCodeSystemVisitor.INSTANCE.visitAndProcess(_sourceCodeSystem, implProvider, objects);
			}
		}
	}

}
