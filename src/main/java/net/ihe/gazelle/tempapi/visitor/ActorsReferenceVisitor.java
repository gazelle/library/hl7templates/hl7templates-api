package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ActorsReferenceProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ActorReference;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ActorsReference;

public class ActorsReferenceVisitor implements HL7TemplateVisitor<ActorsReference> {

	public static final ActorsReferenceVisitor INSTANCE = new ActorsReferenceVisitor();

	@Override
	public void visitAndProcess(ActorsReference t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ActorsReferenceProcessor processor = implProvider.provideImpl(ActorsReferenceProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process actor
		if (t.getActor() != null) {
			for(ActorReference _actorReference : t.getActor()) {
				ActorReferenceVisitor.INSTANCE.visitAndProcess(_actorReference, implProvider, objects);
			}
		}
	}

}
