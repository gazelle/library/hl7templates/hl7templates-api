package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ChoiceDefinitionProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ChoiceDefinitionUtil;

public class ChoiceDefinitionVisitor implements HL7TemplateVisitor<ChoiceDefinition> {

	public static final ChoiceDefinitionVisitor INSTANCE = new ChoiceDefinitionVisitor();

	@Override
	public void visitAndProcess(ChoiceDefinition t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ChoiceDefinitionProcessor processor = implProvider.provideImpl(ChoiceDefinitionProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process constraint
		if (ChoiceDefinitionUtil.getConstraints(t) != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : ChoiceDefinitionUtil.getConstraints(t)) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process desc
		if (t.getDesc() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getDesc()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process element
		if (ChoiceDefinitionUtil.getElements(t) != null) {
			for(RuleDefinition _ruleDefinition : ChoiceDefinitionUtil.getElements(t)) {
				RuleDefinitionVisitor.INSTANCE.visitAndProcess(_ruleDefinition, implProvider, objects);
			}
		}
		// process include
		if (ChoiceDefinitionUtil.getIncludes(t) != null) {
			for(IncludeDefinition _includeDefinition : ChoiceDefinitionUtil.getIncludes(t)) {
				IncludeDefinitionVisitor.INSTANCE.visitAndProcess(_includeDefinition, implProvider, objects);
			}
		}
		

		// process contain
		if (ChoiceDefinitionUtil.getContains(t) != null) {
			for(ContainDefinition _containDefinition : ChoiceDefinitionUtil.getContains(t)) {
				ContainDefinitionVisitor.INSTANCE.visitAndProcess(_containDefinition, implProvider, objects);
			}
		}
		
		
		// process item
		if (t.getItem() != null) {
			ItemVisitor.INSTANCE.visitAndProcess(t.getItem(), implProvider, objects);
		}
	}

}
