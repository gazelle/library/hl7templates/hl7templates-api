package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ProjectHistoryProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ProjectHistory;

public class ProjectHistoryVisitor implements HL7TemplateVisitor<ProjectHistory> {

	public static final ProjectHistoryVisitor INSTANCE = new ProjectHistoryVisitor();

	@Override
	public void visitAndProcess(ProjectHistory t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ProjectHistoryProcessor processor = implProvider.provideImpl(ProjectHistoryProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process desc
		if (t.getDesc() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getDesc()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
	}

}
