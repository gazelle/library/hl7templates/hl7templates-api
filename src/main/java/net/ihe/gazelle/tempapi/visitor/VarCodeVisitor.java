package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.VarCodeProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.VarCode;

public class VarCodeVisitor implements HL7TemplateVisitor<VarCode> {

	public static final VarCodeVisitor INSTANCE = new VarCodeVisitor();

	@Override
	public void visitAndProcess(VarCode t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		VarCodeProcessor processor = implProvider.provideImpl(VarCodeProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
