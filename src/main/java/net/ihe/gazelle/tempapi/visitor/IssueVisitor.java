package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.IssueProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Issue;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueAssignment;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueObject;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueTracking;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.IssueType1Util;

public class IssueVisitor implements HL7TemplateVisitor<Issue> {

	public static final IssueVisitor INSTANCE = new IssueVisitor();

	@Override
	public void visitAndProcess(Issue t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		IssueProcessor processor = implProvider.provideImpl(IssueProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process assignment
		if (IssueType1Util.getAssignments(t) != null) {
			for(IssueAssignment _issueAssignment : IssueType1Util.getAssignments(t)) {
				IssueAssignmentVisitor.INSTANCE.visitAndProcess(_issueAssignment, implProvider, objects);
			}
		}
		// process object
		if (t.getObject() != null) {
			for(IssueObject _issueObject : t.getObject()) {
				IssueObjectVisitor.INSTANCE.visitAndProcess(_issueObject, implProvider, objects);
			}
		}
		// process tracking
		if (IssueType1Util.getTrackings(t) != null) {
			for(IssueTracking _issueTracking : IssueType1Util.getTrackings(t)) {
				IssueTrackingVisitor.INSTANCE.visitAndProcess(_issueTracking, implProvider, objects);
			}
		}
	}

}
