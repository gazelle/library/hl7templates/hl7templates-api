package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ActorReferenceProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ActorReference;

public class ActorReferenceVisitor implements HL7TemplateVisitor<ActorReference> {

	public static final ActorReferenceVisitor INSTANCE = new ActorReferenceVisitor();

	@Override
	public void visitAndProcess(ActorReference t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ActorReferenceProcessor processor = implProvider.provideImpl(ActorReferenceProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
