package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.TemplateIdProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateId;

public class TemplateIdVisitor implements HL7TemplateVisitor<TemplateId> {

	public static final TemplateIdVisitor INSTANCE = new TemplateIdVisitor();

	@Override
	public void visitAndProcess(TemplateId t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		TemplateIdProcessor processor = implProvider.provideImpl(TemplateIdProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
