package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.CopyrightProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.AddrLine;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Copyright;

public class CopyrightVisitor implements HL7TemplateVisitor<Copyright> {

	public static final CopyrightVisitor INSTANCE = new CopyrightVisitor();

	@Override
	public void visitAndProcess(Copyright t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		CopyrightProcessor processor = implProvider.provideImpl(CopyrightProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process addrLine
		if (t.getAddrLine() != null) {
			for(AddrLine _addrLine : t.getAddrLine()) {
				AddrLineVisitor.INSTANCE.visitAndProcess(_addrLine, implProvider, objects);
			}
		}
	}

}
