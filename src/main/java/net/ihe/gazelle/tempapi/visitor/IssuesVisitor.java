package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.IssuesProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Issue;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Issues;

public class IssuesVisitor implements HL7TemplateVisitor<Issues> {

	public static final IssuesVisitor INSTANCE = new IssuesVisitor();

	@Override
	public void visitAndProcess(Issues t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		IssuesProcessor processor = implProvider.provideImpl(IssuesProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process issue
		if (t.getIssue() != null) {
			for(Issue _issue : t.getIssue()) {
				IssueVisitor.INSTANCE.visitAndProcess(_issue, implProvider, objects);
			}
		}
		// process labels
		if (t.getLabels() != null) {
			LabelsVisitor.INSTANCE.visitAndProcess(t.getLabels(), implProvider, objects);
		}
	}

}
