package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ProjectProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Author;
import net.ihe.gazelle.tempmodel.org.decor.art.model.BuildingBlockRepository;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Contact;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Copyright;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Project;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ProjectHistory;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ProjectRelease;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RestURI;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ProjectTypeUtil;

public class ProjectVisitor implements HL7TemplateVisitor<Project> {

	public static final ProjectVisitor INSTANCE = new ProjectVisitor();

	@Override
	public void visitAndProcess(Project t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ProjectProcessor processor = implProvider.provideImpl(ProjectProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process author
		if (t.getAuthor() != null) {
			for(Author _author : t.getAuthor()) {
				AuthorVisitor.INSTANCE.visitAndProcess(_author, implProvider, objects);
			}
		}
		// process buildingBlockRepository
		if (t.getBuildingBlockRepository() != null) {
			for(BuildingBlockRepository _buildingBlockRepository : t.getBuildingBlockRepository()) {
				BuildingBlockRepositoryVisitor.INSTANCE.visitAndProcess(_buildingBlockRepository, implProvider, objects);
			}
		}
		// process contact
		if (t.getContact() != null) {
			for(Contact _contact : t.getContact()) {
				ContactVisitor.INSTANCE.visitAndProcess(_contact, implProvider, objects);
			}
		}
		// process copyright
		if (t.getCopyright() != null) {
			for(Copyright _copyright : t.getCopyright()) {
				CopyrightVisitor.INSTANCE.visitAndProcess(_copyright, implProvider, objects);
			}
		}
		// process defaultElementNamespace
		if (t.getDefaultElementNamespace() != null) {
			DefaultElementNamespaceVisitor.INSTANCE.visitAndProcess(t.getDefaultElementNamespace(), implProvider, objects);
		}
		// process desc
		if (t.getDesc() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getDesc()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process reference
		if (t.getReference() != null) {
			ReferenceVisitor.INSTANCE.visitAndProcess(t.getReference(), implProvider, objects);
		}
		// process release
		if (ProjectTypeUtil.getReleases(t) != null) {
			for(ProjectRelease _projectRelease : ProjectTypeUtil.getReleases(t)) {
				ProjectReleaseVisitor.INSTANCE.visitAndProcess(_projectRelease, implProvider, objects);
			}
		}
		// process restURI
		if (t.getRestURI() != null) {
			for(RestURI _restURI : t.getRestURI()) {
				RestURIVisitor.INSTANCE.visitAndProcess(_restURI, implProvider, objects);
			}
		}
		// process version
		if (ProjectTypeUtil.getVersions(t) != null) {
			for(ProjectHistory _projectHistory : ProjectTypeUtil.getVersions(t)) {
				ProjectHistoryVisitor.INSTANCE.visitAndProcess(_projectHistory, implProvider, objects);
			}
		}
	}

}
