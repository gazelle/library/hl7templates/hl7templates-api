package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.DataSetConceptListProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptList;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptListConcept;

public class DataSetConceptListVisitor implements HL7TemplateVisitor<DataSetConceptList> {

	public static final DataSetConceptListVisitor INSTANCE = new DataSetConceptListVisitor();

	@Override
	public void visitAndProcess(DataSetConceptList t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		DataSetConceptListProcessor processor = implProvider.provideImpl(DataSetConceptListProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process concept
		if (t.getConcept() != null) {
			for(DataSetConceptListConcept _dataSetConceptListConcept : t.getConcept()) {
				DataSetConceptListConceptVisitor.INSTANCE.visitAndProcess(_dataSetConceptListConcept, implProvider, objects);
			}
		}
	}

}
