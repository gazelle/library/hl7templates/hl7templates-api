package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ItemProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Item;

public class ItemVisitor implements HL7TemplateVisitor<Item> {

	public static final ItemVisitor INSTANCE = new ItemVisitor();

	@Override
	public void visitAndProcess(Item t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ItemProcessor processor = implProvider.provideImpl(ItemProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process desc
		if (t.getDesc() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getDesc()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
	}

}
