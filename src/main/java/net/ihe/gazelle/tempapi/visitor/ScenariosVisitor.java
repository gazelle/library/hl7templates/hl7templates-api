package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ScenariosProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Instances;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Scenario;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Scenarios;

public class ScenariosVisitor implements HL7TemplateVisitor<Scenarios> {

	public static final ScenariosVisitor INSTANCE = new ScenariosVisitor();

	@Override
	public void visitAndProcess(Scenarios t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ScenariosProcessor processor = implProvider.provideImpl(ScenariosProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process actors
		if (t.getActors() != null) {
			ActorsDefinitionVisitor.INSTANCE.visitAndProcess(t.getActors(), implProvider, objects);
		}
		// process instances
		if (t.getInstances() != null) {
			for(Instances _instances : t.getInstances()) {
				InstancesVisitor.INSTANCE.visitAndProcess(_instances, implProvider, objects);
			}
		}
		// process scenario
		if (t.getScenario() != null) {
			for(Scenario _scenario : t.getScenario()) {
				ScenarioVisitor.INSTANCE.visitAndProcess(_scenario, implProvider, objects);
			}
		}
	}

}
