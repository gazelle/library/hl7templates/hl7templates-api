package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.InheritDefinitionProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.InheritDefinition;

public class InheritDefinitionVisitor implements HL7TemplateVisitor<InheritDefinition> {

	public static final InheritDefinitionVisitor INSTANCE = new InheritDefinitionVisitor();

	@Override
	public void visitAndProcess(InheritDefinition t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		InheritDefinitionProcessor processor = implProvider.provideImpl(InheritDefinitionProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
