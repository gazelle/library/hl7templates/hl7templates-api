package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ImplementationProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConcept.Implementation;

public class ImplementationVisitor implements HL7TemplateVisitor<Implementation> {

	public static final ImplementationVisitor INSTANCE = new ImplementationVisitor();

	@Override
	public void visitAndProcess(Implementation t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ImplementationProcessor processor = implProvider.provideImpl(ImplementationProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
