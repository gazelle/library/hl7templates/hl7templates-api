package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.DecorProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Decor;

public class DecorVisitor implements HL7TemplateVisitor<Decor> {

	public static final DecorVisitor INSTANCE = new DecorVisitor();

	@Override
	public void visitAndProcess(Decor t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		DecorProcessor processor = implProvider.provideImpl(DecorProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process datasets
		if (t.getDatasets() != null) {
			DatasetsVisitor.INSTANCE.visitAndProcess(t.getDatasets(), implProvider, objects);
		}
		// process ids
		if (t.getIds() != null) {
			IdsVisitor.INSTANCE.visitAndProcess(t.getIds(), implProvider, objects);
		}
		// process issues
		if (t.getIssues() != null) {
			IssuesVisitor.INSTANCE.visitAndProcess(t.getIssues(), implProvider, objects);
		}
		// process project
		if (t.getProject() != null) {
			ProjectVisitor.INSTANCE.visitAndProcess(t.getProject(), implProvider, objects);
		}
		// process rules
		if (t.getRules() != null) {
			RulesVisitor.INSTANCE.visitAndProcess(t.getRules(), implProvider, objects);
		}
		// process scenarios
		if (t.getScenarios() != null) {
			ScenariosVisitor.INSTANCE.visitAndProcess(t.getScenarios(), implProvider, objects);
		}
		// process terminology
		if (t.getTerminology() != null) {
			TerminologyVisitor.INSTANCE.visitAndProcess(t.getTerminology(), implProvider, objects);
		}
	}

}
