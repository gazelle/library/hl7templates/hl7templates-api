package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.CodedConceptProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.CodedConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Designation;

public class CodedConceptVisitor implements HL7TemplateVisitor<CodedConcept> {

	public static final CodedConceptVisitor INSTANCE = new CodedConceptVisitor();

	@Override
	public void visitAndProcess(CodedConcept t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		CodedConceptProcessor processor = implProvider.provideImpl(CodedConceptProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process designation
		if (t.getDesignation() != null) {
			for(Designation _designation : t.getDesignation()) {
				DesignationVisitor.INSTANCE.visitAndProcess(_designation, implProvider, objects);
			}
		}
	}

}
