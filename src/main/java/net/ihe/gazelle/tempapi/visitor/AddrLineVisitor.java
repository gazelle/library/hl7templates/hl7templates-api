package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.AddrLineProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.AddrLine;

public class AddrLineVisitor implements HL7TemplateVisitor<AddrLine> {

	public static final AddrLineVisitor INSTANCE = new AddrLineVisitor();

	@Override
	public void visitAndProcess(AddrLine t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		AddrLineProcessor processor = implProvider.provideImpl(AddrLineProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
