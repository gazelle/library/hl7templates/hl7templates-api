package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.RuleDefinitionProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ChoiceDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ContainDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DefineVariable;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Example;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Let;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Property;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Report;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RuleDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Vocabulary;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RuleDefinitionUtil;

public class RuleDefinitionVisitor implements HL7TemplateVisitor<RuleDefinition> {

	public static final RuleDefinitionVisitor INSTANCE = new RuleDefinitionVisitor();

	@Override
	public void visitAndProcess(RuleDefinition t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		RuleDefinitionProcessor processor = implProvider.provideImpl(RuleDefinitionProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process _assert
		if (RuleDefinitionUtil.getAsserts(t) != null) {
			for(Assert _assert : RuleDefinitionUtil.getAsserts(t)) {
				AssertVisitor.INSTANCE.visitAndProcess(_assert, implProvider, objects);
			}
		}
		// process attribute
		if (t.getAttribute() != null) {
			for(Attribute _attribute : t.getAttribute()) {
				AttributeVisitor.INSTANCE.visitAndProcess(_attribute, implProvider, objects);
			}
		}
		// process choice
		if (RuleDefinitionUtil.getChoices(t) != null) {
			for(ChoiceDefinition _choiceDefinition : RuleDefinitionUtil.getChoices(t)) {
				ChoiceDefinitionVisitor.INSTANCE.visitAndProcess(_choiceDefinition, implProvider, objects);
			}
		}
		// process constraint
		if (RuleDefinitionUtil.getConstraints(t) != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : RuleDefinitionUtil.getConstraints(t)) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process defineVariable
		if (RuleDefinitionUtil.getDefineVariables(t) != null) {
			for(DefineVariable _defineVariable : RuleDefinitionUtil.getDefineVariables(t)) {
				DefineVariableVisitor.INSTANCE.visitAndProcess(_defineVariable, implProvider, objects);
			}
		}
		// process desc
		if (t.getDesc() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getDesc()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process element
		if (RuleDefinitionUtil.getElements(t) != null) {
			for(RuleDefinition _ruleDefinition : RuleDefinitionUtil.getElements(t)) {
				RuleDefinitionVisitor.INSTANCE.visitAndProcess(_ruleDefinition, implProvider, objects);
			}
		}
		// process example
		if (t.getExample() != null) {
			for(Example _example : t.getExample()) {
				ExampleVisitor.INSTANCE.visitAndProcess(_example, implProvider, objects);
			}
		}
		// process include
		if (RuleDefinitionUtil.getIncludes(t) != null) {
			for(IncludeDefinition _includeDefinition : RuleDefinitionUtil.getIncludes(t)) {
				IncludeDefinitionVisitor.INSTANCE.visitAndProcess(_includeDefinition, implProvider, objects);
			}
		}
		
		// process contain
		if (RuleDefinitionUtil.getContains(t) != null) {
			for(ContainDefinition _containDefinition : RuleDefinitionUtil.getContains(t)) {
				ContainDefinitionVisitor.INSTANCE.visitAndProcess(_containDefinition, implProvider, objects);
			}
		}
		
		// process item
		if (t.getItem() != null) {
			ItemVisitor.INSTANCE.visitAndProcess(t.getItem(), implProvider, objects);
		}
		// process let
		if (RuleDefinitionUtil.getLets(t) != null) {
			for(Let _let : RuleDefinitionUtil.getLets(t)) {
				LetVisitor.INSTANCE.visitAndProcess(_let, implProvider, objects);
			}
		}
		// process property
		if (t.getProperty() != null) {
			for(Property _property : t.getProperty()) {
				PropertyVisitor.INSTANCE.visitAndProcess(_property, implProvider, objects);
			}
		}
		// process report
		if (RuleDefinitionUtil.getReports(t) != null) {
			for(Report _report : RuleDefinitionUtil.getReports(t)) {
				ReportVisitor.INSTANCE.visitAndProcess(_report, implProvider, objects);
			}
		}
		// process vocabulary
		if (t.getVocabulary() != null) {
			for(Vocabulary _vocabulary : t.getVocabulary()) {
				VocabularyVisitor.INSTANCE.visitAndProcess(_vocabulary, implProvider, objects);
			}
		}
	}

}
