package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.CodeSystemReferenceProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.CodeSystemReference;

public class CodeSystemReferenceVisitor implements HL7TemplateVisitor<CodeSystemReference> {

	public static final CodeSystemReferenceVisitor INSTANCE = new CodeSystemReferenceVisitor();

	@Override
	public void visitAndProcess(CodeSystemReference t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		CodeSystemReferenceProcessor processor = implProvider.provideImpl(CodeSystemReferenceProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
