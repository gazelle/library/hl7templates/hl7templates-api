package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ReferenceProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Reference;

public class ReferenceVisitor implements HL7TemplateVisitor<Reference> {

	public static final ReferenceVisitor INSTANCE = new ReferenceVisitor();

	@Override
	public void visitAndProcess(Reference t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ReferenceProcessor processor = implProvider.provideImpl(ReferenceProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
