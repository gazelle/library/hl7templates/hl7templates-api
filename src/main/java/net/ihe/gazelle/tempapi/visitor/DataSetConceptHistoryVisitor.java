package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.DataSetConceptHistoryProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptHistory;

public class DataSetConceptHistoryVisitor implements HL7TemplateVisitor<DataSetConceptHistory> {

	public static final DataSetConceptHistoryVisitor INSTANCE = new DataSetConceptHistoryVisitor();

	@Override
	public void visitAndProcess(DataSetConceptHistory t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		DataSetConceptHistoryProcessor processor = implProvider.provideImpl(DataSetConceptHistoryProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process concept
		if (t.getConcept() != null) {
			for(DataSetConcept _dataSetConcept : t.getConcept()) {
				DataSetConceptVisitor.INSTANCE.visitAndProcess(_dataSetConcept, implProvider, objects);
			}
		}
	}

}
