package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ProjectReleaseProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ProjectRelease;

public class ProjectReleaseVisitor implements HL7TemplateVisitor<ProjectRelease> {

	public static final ProjectReleaseVisitor INSTANCE = new ProjectReleaseVisitor();

	@Override
	public void visitAndProcess(ProjectRelease t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ProjectReleaseProcessor processor = implProvider.provideImpl(ProjectReleaseProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process note
		if (t.getNote() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getNote()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
	}

}
