package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.CardinalityProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Cardinality;

public class CardinalityVisitor implements HL7TemplateVisitor<Cardinality> {

	public static final CardinalityVisitor INSTANCE = new CardinalityVisitor();

	@Override
	public void visitAndProcess(Cardinality t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		CardinalityProcessor processor = implProvider.provideImpl(CardinalityProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
