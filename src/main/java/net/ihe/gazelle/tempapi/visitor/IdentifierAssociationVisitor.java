package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.IdentifierAssociationProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IdentifierAssociation;

public class IdentifierAssociationVisitor implements HL7TemplateVisitor<IdentifierAssociation> {

	public static final IdentifierAssociationVisitor INSTANCE = new IdentifierAssociationVisitor();

	@Override
	public void visitAndProcess(IdentifierAssociation t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		IdentifierAssociationProcessor processor = implProvider.provideImpl(IdentifierAssociationProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
