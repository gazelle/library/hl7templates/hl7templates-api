package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ActorDefinitionProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ActorDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;

public class ActorDefinitionVisitor implements HL7TemplateVisitor<ActorDefinition> {

	public static final ActorDefinitionVisitor INSTANCE = new ActorDefinitionVisitor();

	@Override
	public void visitAndProcess(ActorDefinition t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ActorDefinitionProcessor processor = implProvider.provideImpl(ActorDefinitionProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process desc
		if (t.getDesc() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getDesc()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
	}

}
