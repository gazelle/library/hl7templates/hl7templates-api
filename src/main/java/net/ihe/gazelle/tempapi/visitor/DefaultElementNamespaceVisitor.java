package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.DefaultElementNamespaceProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DefaultElementNamespace;

public class DefaultElementNamespaceVisitor implements HL7TemplateVisitor<DefaultElementNamespace> {

	public static final DefaultElementNamespaceVisitor INSTANCE = new DefaultElementNamespaceVisitor();

	@Override
	public void visitAndProcess(DefaultElementNamespace t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		DefaultElementNamespaceProcessor processor = implProvider.provideImpl(DefaultElementNamespaceProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
