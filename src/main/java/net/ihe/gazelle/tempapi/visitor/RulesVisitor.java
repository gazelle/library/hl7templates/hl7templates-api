package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.RulesProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Rules;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateAssociationDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.RulesUtil;

public class RulesVisitor implements HL7TemplateVisitor<Rules> {

	public static final RulesVisitor INSTANCE = new RulesVisitor();

	@Override
	public void visitAndProcess(Rules t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		RulesProcessor processor = implProvider.provideImpl(RulesProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process template
		if (RulesUtil.getTemplates(t) != null) {
			for(TemplateDefinition _templateDefinition : RulesUtil.getTemplates(t)) {
				TemplateDefinitionVisitor.INSTANCE.visitAndProcess(_templateDefinition, implProvider, objects);
			}
		}
		// process templateAssociation
		if (RulesUtil.getTemplateAssociations(t) != null) {
			for(TemplateAssociationDefinition _templateAssociationDefinition : RulesUtil.getTemplateAssociations(t)) {
				TemplateAssociationDefinitionVisitor.INSTANCE.visitAndProcess(_templateAssociationDefinition, implProvider, objects);
			}
		}
	}

}
