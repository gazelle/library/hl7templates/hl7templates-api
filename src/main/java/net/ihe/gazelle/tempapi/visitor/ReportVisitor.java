package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ReportProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Report;

public class ReportVisitor implements HL7TemplateVisitor<Report> {

	public static final ReportVisitor INSTANCE = new ReportVisitor();

	@Override
	public void visitAndProcess(Report t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ReportProcessor processor = implProvider.provideImpl(ReportProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
