package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ActorsDefinitionProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ActorDefinition;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ActorsDefinition;

public class ActorsDefinitionVisitor implements HL7TemplateVisitor<ActorsDefinition> {

	public static final ActorsDefinitionVisitor INSTANCE = new ActorsDefinitionVisitor();

	@Override
	public void visitAndProcess(ActorsDefinition t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ActorsDefinitionProcessor processor = implProvider.provideImpl(ActorsDefinitionProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process actor
		if (t.getActor() != null) {
			for(ActorDefinition _actorDefinition : t.getActor()) {
				ActorDefinitionVisitor.INSTANCE.visitAndProcess(_actorDefinition, implProvider, objects);
			}
		}
	}

}
