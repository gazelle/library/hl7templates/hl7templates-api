package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ScenarioTemplateConceptProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ScenarioTemplateConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ScenarioTemplateCondition;

public class ScenarioTemplateConceptVisitor implements HL7TemplateVisitor<ScenarioTemplateConcept> {

	public static final ScenarioTemplateConceptVisitor INSTANCE = new ScenarioTemplateConceptVisitor();

	@Override
	public void visitAndProcess(ScenarioTemplateConcept t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ScenarioTemplateConceptProcessor processor = implProvider.provideImpl(ScenarioTemplateConceptProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process condition
		if (t.getCondition() != null) {
			for(ScenarioTemplateCondition _scenarioTemplateCondition : t.getCondition()) {
				ScenarioTemplateConditionVisitor.INSTANCE.visitAndProcess(_scenarioTemplateCondition, implProvider, objects);
			}
		}
	}

}
