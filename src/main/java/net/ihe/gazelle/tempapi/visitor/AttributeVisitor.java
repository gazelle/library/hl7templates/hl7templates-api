package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.AttributeProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Attribute;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Vocabulary;

public class AttributeVisitor implements HL7TemplateVisitor<Attribute> {

	public static final AttributeVisitor INSTANCE = new AttributeVisitor();

	@Override
	public void visitAndProcess(Attribute t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		AttributeProcessor processor = implProvider.provideImpl(AttributeProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process constraint
		if (t.getConstraint() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getConstraint()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process desc
		if (t.getDesc() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getDesc()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process item
		if (t.getItem() != null) {
			ItemVisitor.INSTANCE.visitAndProcess(t.getItem(), implProvider, objects);
		}
		// process vocabulary
		if (t.getVocabulary() != null) {
			for(Vocabulary _vocabulary : t.getVocabulary()) {
				VocabularyVisitor.INSTANCE.visitAndProcess(_vocabulary, implProvider, objects);
			}
		}
	}

}
