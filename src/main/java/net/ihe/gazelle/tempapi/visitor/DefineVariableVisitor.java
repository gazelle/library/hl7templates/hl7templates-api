package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.DefineVariableProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DefineVariable;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.DefineVariableTypeUtil;

public class DefineVariableVisitor implements HL7TemplateVisitor<DefineVariable> {

	public static final DefineVariableVisitor INSTANCE = new DefineVariableVisitor();

	@Override
	public void visitAndProcess(DefineVariable t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		DefineVariableProcessor processor = implProvider.provideImpl(DefineVariableProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process code
		if (DefineVariableTypeUtil.getCodes(t) != null) {
			VarCodeVisitor.INSTANCE.visitAndProcess(DefineVariableTypeUtil.getCodes(t), implProvider, objects);
		}
		// process use
		if (DefineVariableTypeUtil.getUses(t) != null) {
			VarUseVisitor.INSTANCE.visitAndProcess(DefineVariableTypeUtil.getUses(t), implProvider, objects);
		}
	}

}
