package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ArbitraryPropertyTypeProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ArbitraryPropertyType;

public class ArbitraryPropertyTypeVisitor implements HL7TemplateVisitor<ArbitraryPropertyType> {

	public static final ArbitraryPropertyTypeVisitor INSTANCE = new ArbitraryPropertyTypeVisitor();

	@Override
	public void visitAndProcess(ArbitraryPropertyType t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ArbitraryPropertyTypeProcessor processor = implProvider.provideImpl(ArbitraryPropertyTypeProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
