package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.PathnameProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Pathname;

public class PathnameVisitor implements HL7TemplateVisitor<Pathname> {

	public static final PathnameVisitor INSTANCE = new PathnameVisitor();

	@Override
	public void visitAndProcess(Pathname t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		PathnameProcessor processor = implProvider.provideImpl(PathnameProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
