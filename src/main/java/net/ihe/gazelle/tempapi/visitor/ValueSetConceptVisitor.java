package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ValueSetConceptProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConcept;

public class ValueSetConceptVisitor implements HL7TemplateVisitor<ValueSetConcept> {

	public static final ValueSetConceptVisitor INSTANCE = new ValueSetConceptVisitor();

	@Override
	public void visitAndProcess(ValueSetConcept t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ValueSetConceptProcessor processor = implProvider.provideImpl(ValueSetConceptProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process desc
		if (t.getDesc() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getDesc()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
	}

}
