package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.InstancesProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Instances;

public class InstancesVisitor implements HL7TemplateVisitor<Instances> {

	public static final InstancesVisitor INSTANCE = new InstancesVisitor();

	@Override
	public void visitAndProcess(Instances t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		InstancesProcessor processor = implProvider.provideImpl(InstancesProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
