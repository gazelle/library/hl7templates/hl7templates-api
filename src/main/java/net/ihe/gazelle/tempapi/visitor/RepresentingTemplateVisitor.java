package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.RepresentingTemplateProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.RepresentingTemplate;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ScenarioTemplateConcept;

public class RepresentingTemplateVisitor implements HL7TemplateVisitor<RepresentingTemplate> {

	public static final RepresentingTemplateVisitor INSTANCE = new RepresentingTemplateVisitor();

	@Override
	public void visitAndProcess(RepresentingTemplate t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		RepresentingTemplateProcessor processor = implProvider.provideImpl(RepresentingTemplateProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process concept
		if (t.getConcept() != null) {
			for(ScenarioTemplateConcept _scenarioTemplateConcept : t.getConcept()) {
				ScenarioTemplateConceptVisitor.INSTANCE.visitAndProcess(_scenarioTemplateConcept, implProvider, objects);
			}
		}
	}

}
