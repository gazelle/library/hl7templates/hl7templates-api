package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.TerminologyProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.CodeSystem;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Terminology;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TerminologyAssociation;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSet;

public class TerminologyVisitor implements HL7TemplateVisitor<Terminology> {

	public static final TerminologyVisitor INSTANCE = new TerminologyVisitor();

	@Override
	public void visitAndProcess(Terminology t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		TerminologyProcessor processor = implProvider.provideImpl(TerminologyProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process codeSystem
		if (t.getCodeSystem() != null) {
			for(CodeSystem _codeSystem : t.getCodeSystem()) {
				CodeSystemVisitor.INSTANCE.visitAndProcess(_codeSystem, implProvider, objects);
			}
		}
		// process terminologyAssociation
		if (t.getTerminologyAssociation() != null) {
			for(TerminologyAssociation _terminologyAssociation : t.getTerminologyAssociation()) {
				TerminologyAssociationVisitor.INSTANCE.visitAndProcess(_terminologyAssociation, implProvider, objects);
			}
		}
		// process valueSet
		if (t.getValueSet() != null) {
			for(ValueSet _valueSet : t.getValueSet()) {
				ValueSetVisitor.INSTANCE.visitAndProcess(_valueSet, implProvider, objects);
			}
		}
	}

}
