package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.IssueAssignmentProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueAssignment;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.IssueAssignmentUtil;

public class IssueAssignmentVisitor implements HL7TemplateVisitor<IssueAssignment> {

	public static final IssueAssignmentVisitor INSTANCE = new IssueAssignmentVisitor();

	@Override
	public void visitAndProcess(IssueAssignment t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		IssueAssignmentProcessor processor = implProvider.provideImpl(IssueAssignmentProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process author
		if (IssueAssignmentUtil.getAuthors(t) != null) {
			AuthorVisitor.INSTANCE.visitAndProcess(IssueAssignmentUtil.getAuthors(t), implProvider, objects);
		}
		// process desc
		if (IssueAssignmentUtil.getDescs(t) != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : IssueAssignmentUtil.getDescs(t)) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
	}

}
