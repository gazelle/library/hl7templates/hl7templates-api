package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ObjectHistoryProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ObjectHistory;

public class ObjectHistoryVisitor implements HL7TemplateVisitor<ObjectHistory> {

	public static final ObjectHistoryVisitor INSTANCE = new ObjectHistoryVisitor();

	@Override
	public void visitAndProcess(ObjectHistory t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ObjectHistoryProcessor processor = implProvider.provideImpl(ObjectHistoryProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process desc
		if (t.getDesc() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getDesc()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
	}

}
