package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.IdProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Designation;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Id;

public class IdVisitor implements HL7TemplateVisitor<Id> {

	public static final IdVisitor INSTANCE = new IdVisitor();

	@Override
	public void visitAndProcess(Id t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		IdProcessor processor = implProvider.provideImpl(IdProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process designation
		if (t.getDesignation() != null) {
			for(Designation _designation : t.getDesignation()) {
				DesignationVisitor.INSTANCE.visitAndProcess(_designation, implProvider, objects);
			}
		}
	}

}
