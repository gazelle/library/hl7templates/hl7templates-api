package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.DefaultBaseIdProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DefaultBaseId;

public class DefaultBaseIdVisitor implements HL7TemplateVisitor<DefaultBaseId> {

	public static final DefaultBaseIdVisitor INSTANCE = new DefaultBaseIdVisitor();

	@Override
	public void visitAndProcess(DefaultBaseId t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		DefaultBaseIdProcessor processor = implProvider.provideImpl(DefaultBaseIdProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
