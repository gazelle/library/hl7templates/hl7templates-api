package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.TemplateAssociationDefinitionProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateAssociationConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateAssociationDefinition;

public class TemplateAssociationDefinitionVisitor implements HL7TemplateVisitor<TemplateAssociationDefinition> {

	public static final TemplateAssociationDefinitionVisitor INSTANCE = new TemplateAssociationDefinitionVisitor();

	@Override
	public void visitAndProcess(TemplateAssociationDefinition t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		TemplateAssociationDefinitionProcessor processor = implProvider.provideImpl(TemplateAssociationDefinitionProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process concept
		if (t.getConcept() != null) {
			for(TemplateAssociationConcept _templateAssociationConcept : t.getConcept()) {
				TemplateAssociationConceptVisitor.INSTANCE.visitAndProcess(_templateAssociationConcept, implProvider, objects);
			}
		}
	}

}
