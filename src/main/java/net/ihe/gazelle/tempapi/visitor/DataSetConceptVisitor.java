package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.DataSetConceptProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ArbitraryPropertyType;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptHistory;
import net.ihe.gazelle.tempmodel.org.decor.art.model.DataSetConceptValue;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ObjectRelationships;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TerminologyAssociation;

public class DataSetConceptVisitor implements HL7TemplateVisitor<DataSetConcept> {

	public static final DataSetConceptVisitor INSTANCE = new DataSetConceptVisitor();

	@Override
	public void visitAndProcess(DataSetConcept t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		DataSetConceptProcessor processor = implProvider.provideImpl(DataSetConceptProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process comment
		if (t.getComment() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getComment()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process concept
		if (t.getConcept() != null) {
			for(DataSetConcept _dataSetConcept : t.getConcept()) {
				DataSetConceptVisitor.INSTANCE.visitAndProcess(_dataSetConcept, implProvider, objects);
			}
		}
		// process desc
		if (t.getDesc() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getDesc()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process history
		if (t.getHistory() != null) {
			for(DataSetConceptHistory _dataSetConceptHistory : t.getHistory()) {
				DataSetConceptHistoryVisitor.INSTANCE.visitAndProcess(_dataSetConceptHistory, implProvider, objects);
			}
		}
		// process implementation
		if (t.getImplementation() != null) {
			ImplementationVisitor.INSTANCE.visitAndProcess(t.getImplementation(), implProvider, objects);
		}
		// process inherit
		if (t.getInherit() != null) {
			InheritDefinitionVisitor.INSTANCE.visitAndProcess(t.getInherit(), implProvider, objects);
		}
		// process operationalization
		if (t.getOperationalization() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getOperationalization()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process property
		if (t.getProperty() != null) {
			for(ArbitraryPropertyType _arbitraryPropertyType : t.getProperty()) {
				ArbitraryPropertyTypeVisitor.INSTANCE.visitAndProcess(_arbitraryPropertyType, implProvider, objects);
			}
		}
		// process rationale
		if (t.getRationale() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getRationale()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process relationship
		if (t.getRelationship() != null) {
			for(ObjectRelationships _objectRelationships : t.getRelationship()) {
				ObjectRelationshipsVisitor.INSTANCE.visitAndProcess(_objectRelationships, implProvider, objects);
			}
		}
		// process source
		if (t.getSource() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getSource()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process terminologyAssociation
		if (t.getTerminologyAssociation() != null) {
			for(TerminologyAssociation _terminologyAssociation : t.getTerminologyAssociation()) {
				TerminologyAssociationVisitor.INSTANCE.visitAndProcess(_terminologyAssociation, implProvider, objects);
			}
		}
		// process valueDomain
		if (t.getValueDomain() != null) {
			for(DataSetConceptValue _dataSetConceptValue : t.getValueDomain()) {
				DataSetConceptValueVisitor.INSTANCE.visitAndProcess(_dataSetConceptValue, implProvider, objects);
			}
		}
	}

}
