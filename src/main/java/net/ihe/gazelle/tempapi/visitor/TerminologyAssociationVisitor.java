package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.TerminologyAssociationProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TerminologyAssociation;

public class TerminologyAssociationVisitor implements HL7TemplateVisitor<TerminologyAssociation> {

	public static final TerminologyAssociationVisitor INSTANCE = new TerminologyAssociationVisitor();

	@Override
	public void visitAndProcess(TerminologyAssociation t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		TerminologyAssociationProcessor processor = implProvider.provideImpl(TerminologyAssociationProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
