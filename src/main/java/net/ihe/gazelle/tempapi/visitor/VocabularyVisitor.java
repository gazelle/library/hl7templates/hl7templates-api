package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.VocabularyProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Vocabulary;

public class VocabularyVisitor implements HL7TemplateVisitor<Vocabulary> {

	public static final VocabularyVisitor INSTANCE = new VocabularyVisitor();

	@Override
	public void visitAndProcess(Vocabulary t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		VocabularyProcessor processor = implProvider.provideImpl(VocabularyProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
