package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ObjectRelationshipsProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ObjectRelationships;

public class ObjectRelationshipsVisitor implements HL7TemplateVisitor<ObjectRelationships> {

	public static final ObjectRelationshipsVisitor INSTANCE = new ObjectRelationshipsVisitor();

	@Override
	public void visitAndProcess(ObjectRelationships t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ObjectRelationshipsProcessor processor = implProvider.provideImpl(ObjectRelationshipsProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
