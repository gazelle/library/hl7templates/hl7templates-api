package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.DatasetsProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Dataset;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Datasets;

public class DatasetsVisitor implements HL7TemplateVisitor<Datasets> {

	public static final DatasetsVisitor INSTANCE = new DatasetsVisitor();

	@Override
	public void visitAndProcess(Datasets t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		DatasetsProcessor processor = implProvider.provideImpl(DatasetsProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process dataset
		if (t.getDataset() != null) {
			for(Dataset _dataset : t.getDataset()) {
				DatasetVisitor.INSTANCE.visitAndProcess(_dataset, implProvider, objects);
			}
		}
	}

}
