package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.TemplatePropertiesProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.TemplateProperties;

public class TemplatePropertiesVisitor implements HL7TemplateVisitor<TemplateProperties> {

	public static final TemplatePropertiesVisitor INSTANCE = new TemplatePropertiesVisitor();

	@Override
	public void visitAndProcess(TemplateProperties t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		TemplatePropertiesProcessor processor = implProvider.provideImpl(TemplatePropertiesProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
