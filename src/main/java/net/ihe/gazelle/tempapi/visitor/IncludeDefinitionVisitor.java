package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.IncludeDefinitionProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Example;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IncludeDefinition;

public class IncludeDefinitionVisitor implements HL7TemplateVisitor<IncludeDefinition> {

	public static final IncludeDefinitionVisitor INSTANCE = new IncludeDefinitionVisitor();

	@Override
	public void visitAndProcess(IncludeDefinition t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		IncludeDefinitionProcessor processor = implProvider.provideImpl(IncludeDefinitionProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process constraint
		if (t.getConstraint() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getConstraint()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process desc
		if (t.getDesc() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getDesc()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
		// process example
		if (t.getExample() != null) {
			for(Example _example : t.getExample()) {
				ExampleVisitor.INSTANCE.visitAndProcess(_example, implProvider, objects);
			}
		}
		// process item
		if (t.getItem() != null) {
			ItemVisitor.INSTANCE.visitAndProcess(t.getItem(), implProvider, objects);
		}
	}

}
