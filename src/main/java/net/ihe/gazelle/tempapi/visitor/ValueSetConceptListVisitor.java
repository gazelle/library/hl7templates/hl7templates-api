package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.ValueSetConceptListProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConcept;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetConceptList;
import net.ihe.gazelle.tempmodel.org.decor.art.model.ValueSetRef;
import net.ihe.gazelle.tempmodel.org.decor.art.utils.ValueSetConceptListUtil;

public class ValueSetConceptListVisitor implements HL7TemplateVisitor<ValueSetConceptList> {

	public static final ValueSetConceptListVisitor INSTANCE = new ValueSetConceptListVisitor();

	@Override
	public void visitAndProcess(ValueSetConceptList t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		ValueSetConceptListProcessor processor = implProvider.provideImpl(ValueSetConceptListProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process concept
		if (ValueSetConceptListUtil.getConcepts(t) != null) {
			for(ValueSetConcept _valueSetConcept : ValueSetConceptListUtil.getConcepts(t)) {
				ValueSetConceptVisitor.INSTANCE.visitAndProcess(_valueSetConcept, implProvider, objects);
			}
		}
		// process exception
		if (t.getException() != null) {
			for(ValueSetConcept _valueSetConcept : t.getException()) {
				ValueSetConceptVisitor.INSTANCE.visitAndProcess(_valueSetConcept, implProvider, objects);
			}
		}
		// process include
		if (ValueSetConceptListUtil.getIncludes(t) != null) {
			for(ValueSetRef _valueSetRef : ValueSetConceptListUtil.getIncludes(t)) {
				ValueSetRefVisitor.INSTANCE.visitAndProcess(_valueSetRef, implProvider, objects);
			}
		}
	}

}
