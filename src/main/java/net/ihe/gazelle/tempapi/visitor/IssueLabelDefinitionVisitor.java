package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.IssueLabelDefinitionProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;
import net.ihe.gazelle.tempmodel.org.decor.art.model.IssueLabelDefinition;

public class IssueLabelDefinitionVisitor implements HL7TemplateVisitor<IssueLabelDefinition> {

	public static final IssueLabelDefinitionVisitor INSTANCE = new IssueLabelDefinitionVisitor();

	@Override
	public void visitAndProcess(IssueLabelDefinition t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		IssueLabelDefinitionProcessor processor = implProvider.provideImpl(IssueLabelDefinitionProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
		// process desc
		if (t.getDesc() != null) {
			for(FreeFormMarkupWithLanguage _freeFormMarkupWithLanguage : t.getDesc()) {
				FreeFormMarkupWithLanguageVisitor.INSTANCE.visitAndProcess(_freeFormMarkupWithLanguage, implProvider, objects);
			}
		}
	}

}
