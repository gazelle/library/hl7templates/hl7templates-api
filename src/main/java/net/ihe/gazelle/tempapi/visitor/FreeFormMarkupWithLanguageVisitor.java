package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.FreeFormMarkupWithLanguageProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.FreeFormMarkupWithLanguage;

public class FreeFormMarkupWithLanguageVisitor implements HL7TemplateVisitor<FreeFormMarkupWithLanguage> {

	public static final FreeFormMarkupWithLanguageVisitor INSTANCE = new FreeFormMarkupWithLanguageVisitor();

	@Override
	public void visitAndProcess(FreeFormMarkupWithLanguage t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		FreeFormMarkupWithLanguageProcessor processor = implProvider.provideImpl(FreeFormMarkupWithLanguageProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
