package net.ihe.gazelle.tempapi.visitor;

import net.ihe.gazelle.tempapi.interfaces.AssertProcessor;
import net.ihe.gazelle.tempapi.utils.HL7TemplateVisitor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempmodel.org.decor.art.model.Assert;

public class AssertVisitor implements HL7TemplateVisitor<Assert> {

	public static final AssertVisitor INSTANCE = new AssertVisitor();

	@Override
	public void visitAndProcess(Assert t, ImplProvider implProvider, Object... objects) {
		if (t == null) return;
		AssertProcessor processor = implProvider.provideImpl(AssertProcessor.class);
		if (processor != null) {
			processor.process(t, objects);
		}
	}

}
