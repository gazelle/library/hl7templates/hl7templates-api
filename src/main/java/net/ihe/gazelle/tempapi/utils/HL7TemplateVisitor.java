package net.ihe.gazelle.tempapi.utils;

public interface HL7TemplateVisitor<T> {
	
	public void visitAndProcess(T obj, ImplProvider implProvider, Object... objects);

}
