package net.ihe.gazelle.tempapi.test;

import static org.junit.Assert.assertTrue;

import net.ihe.gazelle.tempapi.impl.RuleDefinitionProcessorImpl;
import net.ihe.gazelle.tempapi.interfaces.RuleDefinitionProcessor;
import net.ihe.gazelle.tempapi.utils.ImplProvider;
import net.ihe.gazelle.tempapi.utils.Processor;

import org.junit.Before;
import org.junit.Test;

public class ImplProviderTest {
	
	private ImplProvider impl;
	
	@Before
	public void setup() {
		impl = new ImplProvider() {
		
			@Override
			public <T extends Processor> T provideImpl(Class<T> t) {
				if (t.equals(RuleDefinitionProcessor.class)){
					return (T) new RuleDefinitionProcessorImpl() {};
				}
				return null;
			}
		};
	}

	@Test
	public void testImplProvider() {
		RuleDefinitionProcessor aa = impl.provideImpl(RuleDefinitionProcessor.class);
		assertTrue(aa != null);
	}

}
